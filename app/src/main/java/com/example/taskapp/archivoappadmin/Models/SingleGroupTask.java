package com.example.taskapp.archivoappadmin.Models;

public class SingleGroupTask {
    private String userUID, groupTaskID, TaskContent, groupId, status;
    private long deadline, startTime, finishTime;

    public SingleGroupTask() {
    }

    public String getGroupTaskID() {
        return groupTaskID;
    }

    public void setGroupTaskID(String groupTaskID) {
        this.groupTaskID = groupTaskID;
    }

    public String getGroupId() {
        return groupId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getUserUID() {
        return userUID;
    }

    public void setUserUID(String userUID) {
        this.userUID = userUID;
    }


    public String getTaskContent() {
        return TaskContent;
    }

    public void setTaskContent(String taskContent) {
        TaskContent = taskContent;
    }

    public long getDeadline() {
        return deadline;
    }

    public void setDeadline(long deadline) {
        this.deadline = deadline;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(long finishTime) {
        this.finishTime = finishTime;
    }

    @Override
    public String toString() {
        return "SingleGroupTask{" +
                "userUID='" + userUID + '\'' +
                ", groupTaskID='" + groupTaskID + '\'' +
                ", TaskContent='" + TaskContent + '\'' +
                ", groupId='" + groupId + '\'' +
                ", status='" + status + '\'' +
                ", deadline=" + deadline +
                ", startTime=" + startTime +
                ", finishTime=" + finishTime +
                '}';
    }
}
