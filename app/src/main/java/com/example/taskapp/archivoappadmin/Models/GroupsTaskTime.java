package com.example.taskapp.archivoappadmin.Models;

import static com.example.taskapp.archivoappadmin.Common.Common.KEY_STATUS_PENDING;

public class GroupsTaskTime {

    private long startTime, finishTime;
    private String status;


    public GroupsTaskTime() {
        this.startTime = 0;
        this.finishTime = 0;
        this.status = KEY_STATUS_PENDING;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(long finishTime) {
        this.finishTime = finishTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "GroupsTaskTime{" +
                "startTime=" + startTime +
                ", finishTime=" + finishTime +
                ", status='" + status + '\'' +
                '}';
    }
}
