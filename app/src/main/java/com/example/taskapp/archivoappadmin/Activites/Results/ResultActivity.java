package com.example.taskapp.archivoappadmin.Activites.Results;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.taskapp.archivoappadmin.Interface.ItemClickListener;
import com.example.taskapp.archivoappadmin.Models.MyTasks;
import com.example.taskapp.archivoappadmin.R;
import com.example.taskapp.archivoappadmin.ViewHolder.MyTasksViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static com.example.taskapp.archivoappadmin.Common.Common.KEY_QUERY_FILTER_STATUS;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_STATUS_COMPLETED_ON_TIME;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_STATUS_DEFAULT;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_STATUS_LATE_COMPLETE;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_STATUS_PENDING;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_STATUS_STARTED;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_TABLE_TASK;

public class ResultActivity extends AppCompatActivity {

    private static final String TAG = "Results";
    private RecyclerView.LayoutManager layoutManager;


    RecyclerView rv_taskList;

    Spinner sp_Status;
    public TextView tv_count;

    ProgressDialog progressDialog;
    ArrayList<MyTasks> tasks = new ArrayList<>();


    FirebaseRecyclerAdapter<MyTasks, MyTasksViewHolder> adapter;
    FirebaseRecyclerOptions<MyTasks> MyTaskListFirebaseRecyclerOptions;

    String spinner_List[] =
            {
                    KEY_STATUS_DEFAULT,
                    KEY_STATUS_COMPLETED_ON_TIME,
                    KEY_STATUS_LATE_COMPLETE,
                    KEY_STATUS_STARTED,
                    KEY_STATUS_PENDING};

    DatabaseReference databaseReference;
    ValueEventListener resultsValueEventListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        databaseReference = FirebaseDatabase.getInstance()
                .getReference(KEY_TABLE_TASK);


        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);

        sp_Status = findViewById(R.id.sp_filter_spinner);
        tv_count = findViewById(R.id.tvcount);
        sp_Status.setSelection(0);

        rv_taskList = findViewById(R.id.rv_TaskList);
        rv_taskList.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rv_taskList.setLayoutManager(layoutManager);


        sp_Status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                getListOfTasks(spinner_List[i]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                getListOfTasks(KEY_STATUS_DEFAULT);
            }
        });


    }


    private void getListOfTasks(String Condition) {

        Query query;
        if (Condition.equals(spinner_List[0])) {
            query = databaseReference;
        } else {
            query = databaseReference.orderByChild(KEY_QUERY_FILTER_STATUS).equalTo(Condition);
        }

        MyTaskListFirebaseRecyclerOptions = new FirebaseRecyclerOptions.Builder<MyTasks>()
                .setQuery(query, MyTasks.class)
                .build();
        adapter = new FirebaseRecyclerAdapter<MyTasks, MyTasksViewHolder>(MyTaskListFirebaseRecyclerOptions) {
            @Override
            protected void onBindViewHolder(@NonNull MyTasksViewHolder holder, int position, @NonNull final MyTasks model) {
                holder.tv_taskstatus.setText(model.getStatus());
                holder.tv_username.setText(model.getUserName());
                holder.tv_tasksummary.setText(model.getTask());
                holder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        //TODO Fix
                        Toast.makeText(getApplicationContext(), "" + model.toString(), Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @NonNull
            @Override
            public MyTasksViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.rv_task_list_layout, parent, false);


                return new MyTasksViewHolder(itemView);
            }
        };

        rv_taskList.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rv_taskList.setLayoutManager(layoutManager);
        adapter.startListening();
        adapter.notifyDataSetChanged();
        rv_taskList.setAdapter(adapter);


    }

    @Override
    protected void onStart() {
        super.onStart();
        getListOfTasks(KEY_STATUS_DEFAULT);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (adapter != null)
            adapter.stopListening();
    }
}
