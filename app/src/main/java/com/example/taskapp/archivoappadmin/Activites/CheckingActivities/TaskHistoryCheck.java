package com.example.taskapp.archivoappadmin.Activites.CheckingActivities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.taskapp.archivoappadmin.R;

public class TaskHistoryCheck extends AppCompatActivity implements View.OnClickListener {
    private ImageView tv_checkalltask, tv_checkalltaskbyid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_history_check);

        //CheckAllTaskActivity
        tv_checkalltask = findViewById(R.id.iv_CheckAllTasks);
        tv_checkalltaskbyid = findViewById(R.id.iv_CheckTaskByID);

        tv_checkalltask.setOnClickListener(this);
        tv_checkalltaskbyid.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent i;
        switch (view.getId()) {
            case R.id.iv_CheckAllTasks:
                i = new Intent(TaskHistoryCheck.this, CheckAllTaskActivity.class);
                startActivity(i);
                break;
            case R.id.iv_CheckTaskByID:
                i = new Intent(TaskHistoryCheck.this, CheckTaskByIDActivity.class);
                startActivity(i);
                break;

        }
    }
}
