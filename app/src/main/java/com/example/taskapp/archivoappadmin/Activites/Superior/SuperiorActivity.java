package com.example.taskapp.archivoappadmin.Activites.Superior;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.taskapp.archivoappadmin.Adapters.SpinnerArrayAdapter;
import com.example.taskapp.archivoappadmin.Common.Common;
import com.example.taskapp.archivoappadmin.Interface.ItemClickListener;
import com.example.taskapp.archivoappadmin.Models.Groups;
import com.example.taskapp.archivoappadmin.Models.MyTasks;
import com.example.taskapp.archivoappadmin.Models.User;
import com.example.taskapp.archivoappadmin.R;
import com.example.taskapp.archivoappadmin.ViewHolder.UserListViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.example.taskapp.archivoappadmin.Common.Common.GroupList;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_QUERY_FILTER_GROUP_ID;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_QUERY_FILTER_USER_ID;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_QUERY_FILTER_USER_NAME;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_TABLE_TASK;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_TABLE_USERS;

public class SuperiorActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "SuperiorActivity";
    private RecyclerView rv_listOfUsers;
    private RecyclerView.LayoutManager layoutManager;

    private Spinner sp_groups;

    private String old_ID;

    private TextView tv_SelectedUserName,
            tv_personGroupID,
            tv_SelectedUserID,
            tv_name_dialog,
            tv_current_id;

    private ImageView iv_userImage,
            iv_EditUserID,
            iv_Edit_User_picture,
            iv_Edit_User_Name;

    private Button btn_select;

    List<MyTasks> ListOfID = new ArrayList<>();

    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private SpinnerArrayAdapter sp_adapter;
    private FirebaseRecyclerAdapter<User, UserListViewHolder> adapter;
    private FirebaseRecyclerOptions<User> MyTaskListFirebaseRecyclerOptions;

    FirebaseStorage storage;
    StorageReference storageReference;

    TextInputEditText edt_newID;

    ImageView dialog_iv;
    private User selected_user;
    String ImageUrl;

    Uri saveUri;
    private final int PICK_IMAGE_REQUEST = 71;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_superior);

        //init Firebase
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference(KEY_TABLE_USERS);

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference("images/");


        iv_EditUserID = findViewById(R.id.iv_Edit_User_ID);
        iv_EditUserID.setOnClickListener(this);

        iv_Edit_User_picture = findViewById(R.id.iv_Edit_User_picture);
        iv_Edit_User_picture.setOnClickListener(this);


        iv_Edit_User_Name = findViewById(R.id.iv_Edit_User_Name);
        iv_Edit_User_Name.setOnClickListener(this);



        //init Views
        tv_SelectedUserName = findViewById(R.id.tv_userName);

        tv_SelectedUserID = findViewById(R.id.tv_userId);
        tv_personGroupID = findViewById(R.id.tv_personGroupID);
        iv_userImage = findViewById(R.id.iv_userimg);

        if (Common.GroupList == null) {
            Common.GroupList = new ArrayList<>();
        }

        //init Spinner
        sp_groups = findViewById(R.id.sp_groups);
        sp_adapter = new SpinnerArrayAdapter(this, GroupList);
        sp_groups.setAdapter(sp_adapter);
        sp_groups.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SelectedGroup(Common.GroupList.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        //Init Recycle view
        rv_listOfUsers = findViewById(R.id.rv_ListOfUsers);

        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv_listOfUsers.setLayoutManager(layoutManager);
        rv_listOfUsers.setAdapter(adapter);


    }

    private void SelectedGroup(Groups gr) {

        Query query;
        if (!gr.getGroupId().equals("00")) {
            query = databaseReference.orderByChild(KEY_QUERY_FILTER_GROUP_ID).equalTo(gr.getGroupId());
        } else {
            query = databaseReference;
        }

        MyTaskListFirebaseRecyclerOptions = new FirebaseRecyclerOptions.Builder<User>()
                .setQuery(query, User.class)
                .build();

        adapter = new FirebaseRecyclerAdapter<User, UserListViewHolder>(MyTaskListFirebaseRecyclerOptions) {
            @SuppressLint("SetTextI18n")
            @Override
            protected void onBindViewHolder(@NonNull final UserListViewHolder holder, int position, @NonNull final User model) {
                holder.custom_tv_userName.setText("User Name : " + model.getUserName());
                holder.custom_tv_userId.setText("User ID : " + model.getUserId());
                if (model.getImageUrl().equals("")) {
                    Picasso.with(getBaseContext())
                            .load(R.drawable.user_temp)
                            .into(holder.custom_iv_userImg);
                } else {
                    Picasso.with(getBaseContext())
                            .load(model.getImageUrl())
                            .into(holder.custom_iv_userImg);
                }
                holder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {

                        selected_user = model;
                        tv_SelectedUserID.setText("User ID : " + selected_user.getUserId());
                        tv_SelectedUserName.setText("User Name :" + selected_user.getUserName());
                        tv_personGroupID.setText("Group ID : " + Common.getGroupName(selected_user.getGroupId()));
                        if (model.getImageUrl().equals("")) {
                            Picasso.with(getBaseContext())
                                    .load(R.drawable.user_temp)
                                    .into(iv_userImage);
                        } else {
                            Picasso.with(getBaseContext())
                                    .load(model.getImageUrl())
                                    .into(iv_userImage);
                        }

                    }
                });
            }

            @NonNull
            @Override
            public UserListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.rv_perople_row, parent, false);


                return new UserListViewHolder(itemView);
            }
        };

        adapter.startListening();
        adapter.notifyDataSetChanged();

        rv_listOfUsers.setAdapter(adapter);

        if (selected_user != null)


        Log.d(TAG, "SelectedGroup: " + ListOfID.size());
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (adapter != null)
            adapter.stopListening();
    }

    @Override
    public void onClick(View v) {
        if (selected_user != null) {
            switch (v.getId()) {
                case R.id.iv_Edit_User_ID:
                    ShowEditIdDialog(selected_user);
                    break;
                case R.id.iv_Edit_User_Name:
                    ShowEditNameDialog(selected_user);
                    break;
                case R.id.iv_Edit_User_picture:
                    ShowEditPictureDialog(selected_user);
                    break;

            }
        } else {
            Toast.makeText(SuperiorActivity.this, "Please Select User First", Toast.LENGTH_SHORT).show();
        }
    }

    private void ShowEditNameDialog(final User selected_user) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Edit Person Name");
        alertDialog.setMessage("Please Enter new name");
        Log.d(TAG, "ShowEditNameDialog: ");
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogEditPicture = inflater.inflate(R.layout.dialog_edit_name, null);


        final TextInputEditText edt_name = dialogEditPicture.findViewById(R.id.edt_new_name);

        final ImageView iv = dialogEditPicture.findViewById(R.id.iv_userImg);

        if (!selected_user.getImageUrl().equals("")) {
            Picasso.with(this).load(selected_user.getImageUrl()).into(iv);
        }


        Button Submit = dialogEditPicture.findViewById(R.id.btn_submit);

        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO
                String newName = edt_name.getText().toString();
                if (newName.isEmpty()) {
                    edt_name.setError(getString(R.string.input_error_name));
                    edt_name.requestFocus();
                } else {
                    selected_user.setUserName(newName);
                    changeUserName(selected_user);

                }

            }
        });

        alertDialog.setView(dialogEditPicture);


        alertDialog.show();

        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();

            }
        });
    }

    private void changeUserName(final User user) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference(KEY_TABLE_USERS);
        ref.child(user.getUserUid()).child("userName")
                .setValue(user.getUserName())
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(SuperiorActivity.this, "Name changed Sucsesfuly ", Toast.LENGTH_SHORT).show();
                            if (ListOfID.size() > 0) {
                                for (int i = 0; i < ListOfID.size(); i++) {
                                    changeNameToNewTasks(ListOfID.get(i).getUserUid(), user.getUserName());
                                }
                            }


                        } else {
                            Toast.makeText(SuperiorActivity.this, "Error changing name ", Toast.LENGTH_SHORT).show();
                        }
                    }
                });


    }


    private void ShowEditPictureDialog(final User selected_user) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Edit Person Picture");
        alertDialog.setMessage("Please select picture");

        alertDialog.setCancelable(false);

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogEditPicutre = inflater.inflate(R.layout.dialog_edit_picture, null);

        final ImageView iv = dialogEditPicutre.findViewById(R.id.iv_userImg);
        btn_select = dialogEditPicutre.findViewById(R.id.btn_select_image);
        btn_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChoseImage();
                if (saveUri != null) {
                    Picasso.with(alertDialog.getContext())
                            .load(saveUri)
                            .into(iv);
                    updateUserImage(selected_user);
                }
            }
        });
        Button Submit = dialogEditPicutre.findViewById(R.id.btn_submit);
        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadImage();
            }
        });
        alertDialog.setView(dialogEditPicutre);

        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                updateUserImage(selected_user);
            }
        });
        alertDialog.show();

    }

    private void updateUserImage(User selected_user) {
        if (!selected_user.getImageUrl().equals("")) {
            DatabaseReference db = FirebaseDatabase.getInstance().getReference(KEY_TABLE_USERS);
            db.child(selected_user.getUserUid()).child("imageUrl").setValue(selected_user.getImageUrl()).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        Toast.makeText(getApplicationContext(), "Image Uploaded Succesfully", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Image Uploading Failed", Toast.LENGTH_SHORT).show();

                    }

                }
            });

        }
    }


    private void EditUserID(String uid, String newID) {

        DatabaseReference ref = databaseReference;
        ref.child(uid).child(KEY_QUERY_FILTER_USER_ID)
                .setValue(newID)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Log.d(TAG, "onComplete: " + task.isSuccessful());
                    }
                });

    }

    private void uploadImage() {
        Log.d(TAG, "uploadImage: 347");
        if (saveUri != null) {
            final ProgressDialog mDialog = new ProgressDialog(this);
            mDialog.setMessage("Uploading...");
            Log.d(TAG, "uploadImage: 351");
            mDialog.show();
            String ImageName = UUID.randomUUID().toString();
            final StorageReference imageFolder = storageReference.child("Images/" + ImageName);
            imageFolder.putFile(saveUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    mDialog.dismiss();
                    Toast.makeText(SuperiorActivity.this, "Uploaded", Toast.LENGTH_SHORT).show();
                    imageFolder.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            Log.d(TAG, "onSuccess: " + uri.toString());
                            selected_user.setImageUrl(uri.toString());

                        }
                    });
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    mDialog.dismiss();
                    Toast.makeText(SuperiorActivity.this, "Uploading Failed", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "onFailure: " + e.toString());
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                    mDialog.setMessage("Uploaded" + progress + " %");
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST &&
                resultCode == RESULT_OK &&
                data != null &&
                data.getData() != null) {
            saveUri = data.getData();
            btn_select.setText("Image Selected !");
            btn_select.setActivated(false);
        }

    }

    //Method to check if id exists
    private Boolean checkExistID(final String ID, final TextInputEditText edt_newID) {
        final Boolean[] check = {false};
        Query query = databaseReference.orderByChild(KEY_QUERY_FILTER_USER_ID).equalTo(ID);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    User user = ds.getValue(User.class);
                    if (user != null) {
                        if (user.getUserId().equals(ID)) {
                            check[0] = true;
                            edt_newID.setError(getString(R.string.input_error_userID_Exist));
                            edt_newID.requestFocus();
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });

        return check[0];
    }

    @SuppressLint("SetTextI18n")
    private void ShowEditIdDialog(final User user) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Edit Person ID");
        alertDialog.setMessage("Please write new ID");

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogEditID = inflater.inflate(R.layout.dialog_edit_id, null);

        tv_name_dialog = dialogEditID.findViewById(R.id.tv_person_Name);
        tv_name_dialog.setText("User name : " + user.getUserName());

        tv_current_id = dialogEditID.findViewById(R.id.tv_personOldID);
        tv_current_id.setText("User Current ID : " + user.getUserId());

        dialog_iv = dialogEditID.findViewById(R.id.iv_userImg);

        edt_newID = dialogEditID.findViewById(R.id.edt_new_id);

        tv_personGroupID = dialogEditID.findViewById(R.id.tv_personGroupID);
        tv_personGroupID.setText("Group ID : " + Common.getGroupName(user.getGroupId()));
        btn_select = dialogEditID.findViewById(R.id.btn_submit);


        alertDialog.setView(dialogEditID);

        old_ID = user.getUserId();

        btn_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO
                //Check ID Format , Existance ;
                String newID = edt_newID.getText().toString();
                if (Common.checkIdFormat(newID)) {
                    if (!checkExistID(newID, edt_newID)) {
                        EditUserID(user.getUserUid(), newID);
                        if (ListOfID.size() > 0) {
                            for (int i = 0; i < ListOfID.size(); i++) {
                                changeIdToNewTasks(ListOfID.get(i).getUserUid(), newID);
                            }
                        }
                    }
                } else {
                    edt_newID.setError(getString(R.string.input_error_Format_ID));
                    edt_newID.requestFocus();
                }
            }
        });


        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();

            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alertDialog.show();
    }


    //Method to change all Tasks child userId with the user id
    // the specific iD
    //DatabaseReference TableTasks
    //Key The Key for the Task Belonging to user ID
    //newID the value of the new user's id
    private void changeIdToNewTasks(String Key, String newID) {
        DatabaseReference ref = firebaseDatabase.getReference(KEY_TABLE_TASK);
        ref.child(Key).child(KEY_QUERY_FILTER_USER_ID).setValue(newID)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Log.d(TAG, "onComplete: changeIdToNew " + task.isSuccessful());
                    }
                });
    }

    //Method to change all Tasks child userId with the user id
    // the specific iD
    //DatabaseReference TableTasks
    //Key The Key for the Task Belonging to user ID
    //newID the value of the new user's id
    private void changeNameToNewTasks(String Key, String newName) {
        DatabaseReference ref = firebaseDatabase.getReference(KEY_TABLE_TASK);
        ref.child(Key).child(KEY_QUERY_FILTER_USER_NAME).setValue(newName)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Log.d(TAG, "onComplete: changeIdToNew " + task.isSuccessful());
                    }
                });
    }

    //Method to change User ID in User Table
    //DatabaseReference Users
    //Key child in Users Table the user UID
    //newID the value of the new user's id
    private void changeIdToNewUsers(String Key, String newID) {
        DatabaseReference ref = firebaseDatabase.getReference(KEY_TABLE_USERS);
        ref.child(Key).child(KEY_QUERY_FILTER_USER_ID).setValue(newID)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Log.d(TAG, "onComplete: changeIdToNewUsers " + task.isSuccessful());
                    }
                });
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    private void ChoseImage() {
        //Lets the user select an image from galery and save the Uri created in the firebase Storage
        Intent i = new Intent();
        i.setType("image/*");
        i.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(i, "Select Picture"), PICK_IMAGE_REQUEST);
    }

}
