package com.example.taskapp.archivoappadmin.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.taskapp.archivoappadmin.Interface.ItemClickListener;
import com.example.taskapp.archivoappadmin.R;

import de.hdodenhof.circleimageview.CircleImageView;


public class AdminUserListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView tv_user;
    public ImageView iv_profile;
    public CircleImageView iv_active;
    private ItemClickListener itemClickListener;

    public AdminUserListViewHolder(View itemView) {
        super(itemView);
        tv_user = itemView.findViewById(R.id.tvtitle);
        iv_active = itemView.findViewById(R.id.iv_isActive);
        iv_profile = itemView.findViewById(R.id.iv_user_ProfilePic);
        itemView.setOnClickListener(this);
    }


    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view, getAdapterPosition(), false);
    }
}
