package com.example.taskapp.archivoappadmin.Models;

import android.os.Parcel;
import android.os.Parcelable;

public class MyTasks implements Parcelable {

    private String status, task, userName, userUid, userId;
    private long deadline, finishTime, startTime;

    public MyTasks() {
    }

    public MyTasks(String status,
                   String task,
                   String userName,
                   String userId,
                   long deadline,
                   long finishTime,
                   long startTime) {
        this.status = status;
        this.task = task;
        this.userName = userName;
        this.userId = userId;
        this.deadline = deadline;
        this.finishTime = finishTime;
        this.startTime = startTime;
    }

    public MyTasks(String status,
                   String task,
                   String userName,
                   String userId,
                   String userUid,
                   long deadline,
                   long finishTime,
                   long startTime) {
        this.status = status;
        this.task = task;
        this.userName = userName;
        this.userUid = userUid;
        this.userId = userId;
        this.deadline = deadline;
        this.finishTime = finishTime;
        this.startTime = startTime;
    }

    public long getDeadline() {
        return deadline;
    }

    public void setDeadline(long deadline) {
        this.deadline = deadline;
    }

    protected MyTasks(Parcel in) {
        status = in.readString();
        task = in.readString();
        userName = in.readString();
        userUid = in.readString();
        userId = in.readString();
        deadline = in.readLong();
        finishTime = in.readLong();
        startTime = in.readLong();
    }


    public static final Creator<MyTasks> CREATOR = new Creator<MyTasks>() {
        @Override
        public MyTasks createFromParcel(Parcel in) {
            return new MyTasks(in);
        }

        @Override
        public MyTasks[] newArray(int size) {
            return new MyTasks[size];
        }
    };

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }


    public long getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(long finishTime) {
        this.finishTime = finishTime;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public String getUserUid() {
        return userUid;
    }

    public void setUserUid(String userUid) {
        this.userUid = userUid;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }


    @Override
    public String toString() {
        return "MyTasks{" +
                "status='" + status + '\'' +
                ", task='" + task + '\'' +
                ", userName='" + userName + '\'' +
                ", userUid='" + userUid + '\'' +
                ", userId='" + userId + '\'' +
                ", deadline=" + deadline +
                ", finishTime=" + finishTime +
                ", startTime=" + startTime +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
        dest.writeString(task);
        dest.writeString(userName);
        dest.writeString(userUid);
        dest.writeString(userId);
        dest.writeLong(deadline);
        dest.writeLong(finishTime);
        dest.writeLong(startTime);
    }
}
