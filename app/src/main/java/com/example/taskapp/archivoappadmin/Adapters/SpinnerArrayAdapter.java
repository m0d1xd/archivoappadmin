package com.example.taskapp.archivoappadmin.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.taskapp.archivoappadmin.Models.Groups;
import com.example.taskapp.archivoappadmin.R;

import java.util.ArrayList;


public class SpinnerArrayAdapter extends BaseAdapter {

    private final LayoutInflater inflter;
    private final ArrayList<Groups> myList;

    public SpinnerArrayAdapter(Context mContext, ArrayList<Groups> myList) {
        this.inflter = (LayoutInflater.from(mContext));
        this.myList = myList;
    }

    @Override
    public int getCount() {
        if (myList != null)
            return myList.size();
        else return 0;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @SuppressLint({"ViewHolder", "InflateParams"})
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.sp_custom_layout, null);
        TextView names = view.findViewById(R.id.Group_name);
        names.setText(myList.get(i).getGroupName());

        return view;
    }

}
