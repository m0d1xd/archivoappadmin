//package com.example.taskapp.archivoappadmin.Database;
//
//import android.content.ContentValues;
//import android.content.Context;
//import android.database.Cursor;
//import android.database.sqlite.SQLiteDatabase;
//import android.database.sqlite.SQLiteQueryBuilder;
//import android.os.Build;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.support.annotation.RequiresApi;
//import android.util.Log;
//
//import com.example.taskapp.archivoappadmin.Models.GroupsTaskTime;
//import com.example.taskapp.archivoappadmin.Models.GroupsTasks;
//import com.example.taskapp.archivoappadmin.Models.MyTasks;
//import com.example.taskapp.archivoappadmin.Models.SingleGroupTask;
//import com.example.taskapp.archivoappadmin.Models.User;
//import com.google.firebase.database.ChildEventListener;
//import com.google.firebase.database.DataSnapshot;
//import com.google.firebase.database.DatabaseError;
//import com.google.firebase.database.DatabaseReference;
//import com.google.firebase.database.FirebaseDatabase;
//import com.google.firebase.database.ValueEventListener;
//import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import static com.example.taskapp.archivoappadmin.Common.Common.CheckTimerStatus;
//import static com.example.taskapp.archivoappadmin.Common.Common.KEY_STATUS_PENDING;
//import static com.example.taskapp.archivoappadmin.Common.Common.KEY_STATUS_STARTED;
//import static com.example.taskapp.archivoappadmin.Common.Common.KEY_TABLE_GROUPS_TASK;
//import static com.example.taskapp.archivoappadmin.Common.Common.KEY_TABLE_TASK;
//import static com.example.taskapp.archivoappadmin.Common.Common.KEY_TABLE_USERS;
//
//public class Database extends SQLiteAssetHelper {
//
//    private static final String TAG = "Database class";
//
//    static private final String DB_Name = "statisticsOLD.db";
//    static private final int DB_VER = 1;
//
//
//    private static final String TABLE_TASKS = "Tasks";
//    private static final String TABLE_USERS = "Users";
//    private static final String TABLE_GROUP_TASKS = "GroupTasks";
//    private static final String TABLE_STATICS = "Statics";
//
//
//    private static final String KEY_USER_ID = "userId";
//    private static final String KEY_USER_UID = "userUid";
//    private static final String KEY_USER_NAME = "userName";
//    private static final String KEY_USER_GROUP_ID = "groupId";
//
//
//    private static final String KEY_TASK_ID = "TaskId";
//    private static final String KEY_GROUP_TASK_ID = "GroupTaskID";
//    private static final String KEY_TASK_Finish_Time = "finishTime";
//    private static final String KEY_TASK_Start_Time = "startTime";
//    private static final String KEY_TASK_Deadline = "deadline";
//    private static final String KEY_TASK_CONTENT = "task_content";
//
//
//    private static final String KEY_TASK_Status = "status";
//    private static final String KEY_TASK_User_ID = "userId";
//    private static final String KEY_TASK_User_UID = "userUid";
//    private static final String KEY_TASK_user_Name = "userName";
//
//
//    public Database(Context context) {
//        super(context, DB_Name, null, DB_VER);
//
//    }
//
//    public void fillData() {
//
//        ClearAllDatabase();
//
//        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
//        DatabaseReference ref_groupsTasks = firebaseDatabase.getReference(KEY_TABLE_GROUPS_TASK);
//        DatabaseReference ref_IndividualsTasks = firebaseDatabase.getReference(KEY_TABLE_TASK);
//        DatabaseReference ref_Users = firebaseDatabase.getReference(KEY_TABLE_USERS);
//
//
////        ref_Users.addValueEventListener(new ValueEventListener() {
////            @Override
////            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
////                for (DataSnapshot ds : dataSnapshot.getChildren()) {
////                    User user = ds.getValue(User.class);
////                    assert user != null;
////                    if (!CheckUserExist(user.getUserUid())) {
////                        addUser(ds.getValue(User.class));
////                    }
////                }
////            }
////
////            @Override
////            public void onCancelled(@NonNull DatabaseError databaseError) {
////
////            }
////        });
////
////        ref_IndividualsTasks.addValueEventListener(new ValueEventListener() {
////            @Override
////            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
////
////                for (DataSnapshot ds : dataSnapshot.getChildren()) {
////                    MyTasks myTasks = ds.getValue(MyTasks.class);
////                    if (checkTaskExist(ds.getKey())) {
////                        EditTask(myTasks, ds.getKey());
////                    } else {
////                        addToDataBase(ds.getValue(MyTasks.class), ds.getKey());
////                    }
////                }
////            }
////
////            @Override
////            public void onCancelled(@NonNull DatabaseError databaseError) {
////
////            }
////        });
////
////        ref_groupsTasks.addValueEventListener(new ValueEventListener() {
////            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
////            @Override
////            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
////                for (DataSnapshot ds : dataSnapshot.getChildren()) {
////                    GroupsTasks groupsTasks = ds.getValue(GroupsTasks.class);
////                    SingleGroupTask singleGroupTask = new SingleGroupTask();
////                    List<User> users = UsersInGroup(groupsTasks.getGroupId());
////                    for (User user : users) {
////                        singleGroupTask.setGroupTaskID(ds.getKey());
////                        singleGroupTask.setTaskContent(groupsTasks.getTask());
////                        singleGroupTask.setUserUID(user.getUserUid());
////                        singleGroupTask.setGroupId(groupsTasks.getGroupId());
////                        singleGroupTask.setDeadline(groupsTasks.getDeadline());
////                        try {
////                            GroupsTaskTime t = groupsTasks.getTimeTable().get(user.getUserUid());
////                            singleGroupTask.setStartTime(t.getStartTime());
////                            singleGroupTask.setFinishTime(t.getFinishTime());
////                            singleGroupTask.setStatus(t.getStatus());
////                            Log.d(TAG, "onDataChange: " + singleGroupTask.toString());
////
////                            if (checkTaskGroupExist(ds.getKey(), user.getUserUid())) {
////                                //TODO if exist .. UPDATE
////                                Log.d(TAG, "onDataChange: Task Already Exist");
////                            } else {
////                                //ADD
////                           //     AddToGroupTasks(singleGroupTask, ds.getKey());
////
////                            }
////
////
////                        } catch (Exception e) {
////
////                            Log.d(TAG, "onDataChange: " + e.getMessage());
////                        }
////
////                    }
////
////                }
////            }
////
////            @Override
////            public void onCancelled(@NonNull DatabaseError databaseError) {
////
////            }
////        });
//
//        //TODO Fill Statistics data from THe current Data filled in the DB
//
//
//    }
//
//
//    public void printAllDB() {
//
//        SQLiteDatabase db = getReadableDatabase();
//        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
//
//        String[] sqlSelect = {
//                KEY_GROUP_TASK_ID,
//                KEY_TASK_CONTENT,
//                KEY_USER_UID,
//                KEY_TASK_User_ID,
//                KEY_TASK_user_Name,
//                KEY_TASK_Deadline,
//                KEY_TASK_Start_Time,
//                KEY_TASK_Finish_Time,
//                KEY_USER_GROUP_ID,
//                KEY_TASK_Status
//        };
//
//        String sqlTable = TABLE_GROUP_TASKS;
//
//        qb.setTables(sqlTable);
//
//        Cursor c = qb.query(db, sqlSelect, null, null, null, null, null);
//        List<SingleGroupTask> tasks = new ArrayList<>();
//
//        Log.d(TAG, "printAllDB: Started ");
//
//        try {
//            if (c.moveToFirst()) {
//                do {
//                    SingleGroupTask task = new SingleGroupTask();
//                    task.setGroupTaskID(c.getString(c.getColumnIndex(KEY_GROUP_TASK_ID)));
//                    task.setTaskContent(c.getString(c.getColumnIndex(KEY_TASK_CONTENT)));
//                    task.setUserUID(c.getString(c.getColumnIndex(KEY_TASK_User_UID)));
//                    task.setDeadline(Long.parseLong(c.getString(c.getColumnIndex(KEY_TASK_Deadline))));
//                    task.setStartTime(Long.parseLong(c.getString(c.getColumnIndex(KEY_TASK_Start_Time))));
//                    task.setFinishTime(Long.parseLong(c.getString(c.getColumnIndex(KEY_TASK_Finish_Time))));
//                    task.setGroupId(c.getString(c.getColumnIndex(KEY_USER_GROUP_ID)));
//                    task.setStatus(c.getString(c.getColumnIndex(KEY_TASK_Status)));
//
//                    tasks.add(task);
//
//                    Log.d(TAG, "printAllDB: " + task.toString());
//
//                } while (c.moveToNext());
//            }
//
//        } catch (Exception e) {
//            Log.d(TAG, "printAllDB: " + e.getMessage());
//        }
//        Log.d(TAG, "printAllDB: finished ");
//
//
//    }
//
//
//    public void print() {
//
//        // 1. build the query
//        List<SingleGroupTask> Tasks = new ArrayList<>();
//
//        String query = "SELECT  * FROM " + TABLE_GROUP_TASKS;
//
//
//        // 2. get reference to writable DB
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        Cursor c = db.rawQuery(query, null);
//
//
//        // 3. go over each row, build book and add it to list
//        SingleGroupTask task = null;
//
//        if (c.moveToFirst()) {
//            do {
//                task = new SingleGroupTask();
//
//                task.setGroupTaskID(c.getString(1));
//                task.setTaskContent(c.getString(2));
//                task.setUserUID(c.getString(3));
//                task.setDeadline(Long.parseLong(c.getString(6)));
//                task.setStartTime(Long.parseLong(c.getString(7)));
//                task.setFinishTime(Long.parseLong(c.getString(8)));
//                task.setGroupId(c.getString(9));
//                task.setStatus(c.getString(10));
//                Log.d(TAG, "print: " + task.toString());
//            } while (c.moveToNext());
//        }
//
//
//    }
//
//    public List<User> UsersInGroup(String groupId) {
//
//        SQLiteDatabase db = getReadableDatabase();
//        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
//
//        String[] sqlSelect = {
//                KEY_TASK_User_UID,
//                KEY_USER_ID,
//                KEY_USER_NAME,
//                KEY_USER_GROUP_ID
//        };
//
//
//        String sqlTable = TABLE_USERS;
//
//
//        qb.setTables(sqlTable);
//        Cursor c = qb.query(db, sqlSelect, null, null, null, null, null);
//        List<User> users = new ArrayList<>();
//
//        if (c.moveToFirst() && c.getCount() >= 1) {
//            do {
//
//                User user = new User();
//                Log.d(TAG, "UsersInGroup: " + c.getString(c.getColumnIndex(KEY_USER_GROUP_ID)));
//                String db_Group = c.getString(c.getColumnIndex(KEY_USER_GROUP_ID));
//                if (groupId.equals(db_Group)) {
//                    user.setUserUid(c.getString(c.getColumnIndex(KEY_TASK_User_UID)));
//                    user.setUserId(c.getString(c.getColumnIndex(KEY_USER_ID)));
//                    user.setUserName(c.getString(c.getColumnIndex(KEY_USER_NAME)));
//                    user.setGroupId(c.getString(c.getColumnIndex(KEY_USER_GROUP_ID)));
//                    users.add(user);
//                }
//
//            } while (c.moveToNext());
//        }
//        c.close();
//        db.close();
//
//
//        return users;
//    }
//
//
//    //To Add From MyTask object into Internal Database
//    //this must be called in AddTaskToPerson
//    public void addToDataBase(MyTasks myTasks, String Taskid) {
//        //Create and/or open the database for writing
//        SQLiteDatabase db = getReadableDatabase();
//        db.beginTransaction();
//
//        // wrap our insert in a transaction. This helps with performance and ensures
//        // consistency of the database.
//        try {
//            ContentValues values = new ContentValues();
//
//            values.put(KEY_TASK_ID, Taskid);
//            values.put(KEY_TASK_Finish_Time, myTasks.getFinishTime());
//            values.put(KEY_TASK_Start_Time, myTasks.getStartTime());
//            values.put(KEY_TASK_Deadline, myTasks.getDeadline());
//            values.put(KEY_TASK_Status, myTasks.getStatus());
//            values.put(KEY_TASK_CONTENT, myTasks.getTask());
//            values.put(KEY_TASK_user_Name, myTasks.getUserName());
//            values.put(KEY_TASK_User_ID, myTasks.getUserId());
//            values.put(KEY_TASK_User_UID, myTasks.getUserUid());
//
//            // Notice how we haven't specified the primary key. SQLite auto increments the primary key column.
//            db.insertOrThrow(TABLE_TASKS, null, values);
//
//            db.setTransactionSuccessful();
//            //TRANSACTION COMPLETED
//            Log.d(TAG, "addToCart: TRANSACTION COMPLETED " + db.isOpen());
//        } catch (Exception e) {
//            Log.d(TAG, e.toString() + "|" + e.getMessage());
//        } finally {
//            db.endTransaction();
//            db.close();
//            Log.d(TAG, "addToCart: TRANSACTION COMPLETED " + db.isOpen());
//        }
//
//
//    }
//
//
//    private void addUser(User user) {
//        SQLiteDatabase db = getReadableDatabase();
//        db.beginTransaction();
//
//        // wrap our insert in a transaction. This helps with performance and ensures
//        // consistency of the database.
//        try {
//            ContentValues values = new ContentValues();
//
//            values.put(KEY_USER_UID, user.getUserUid());
//            values.put(KEY_USER_ID, user.getUserId());
//            values.put(KEY_USER_NAME, user.getUserName());
//            values.put(KEY_USER_GROUP_ID, user.getGroupId());
//            // Notice how we haven't specified the primary key. SQLite auto increments the primary key column.
//            db.insertOrThrow(TABLE_USERS, null, values);
//            db.setTransactionSuccessful();
//            //TRANSACTION COMPLETED
//            Log.d(TAG, "addToCart: TRANSACTION COMPLETED " + db.isOpen());
//        } catch (Exception e) {
//            Log.d(TAG, e.toString() + "|" + e.getMessage());
//        } finally {
//            db.endTransaction();
//            db.close();
//            Log.d(TAG, "addToCart: TRANSACTION COMPLETED " + db.isOpen());
//        }
//
//    }
//
//    private Boolean CheckUserExist(String key) {
//        SQLiteDatabase db = getReadableDatabase();
//        String query = String.format("SELECT * FROM " + TABLE_USERS + " WHERE " + KEY_TASK_User_UID + "='%s';", key);
//        Cursor c = db.rawQuery(query, null);
//        Log.d(TAG, "checkUserExist: " + c.getCount());
//        return c.getCount() > 0;
//
//    }
//
//
//    public void getAllTasks() {
//        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
//        final DatabaseReference databaseReference = firebaseDatabase.getReference(KEY_TABLE_TASK);
//        databaseReference.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                for (DataSnapshot ds : dataSnapshot.getChildren()) {
//                    MyTasks myTasks = ds.getValue(MyTasks.class);
//                    if (checkTaskExist(ds.getKey())) {
//                        EditTask(myTasks, ds.getKey());
//                        Log.d(TAG, "onDataChange: Exist " + myTasks);
//                    } else {
//                        AddToTasks(myTasks, ds.getKey());
//                        Log.d(TAG, "onDataChange: not Exist " + myTasks);
//                    }
//                }
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });
//
//        databaseReference.addChildEventListener(new ChildEventListener() {
//            @Override
//            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//
//            }
//
//            @Override
//            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//                MyTasks m = dataSnapshot.getValue(MyTasks.class);
//                Log.d(TAG, "onChildChanged: " + m.toString());
//                EditTask(m, dataSnapshot.getKey());
//            }
//
//            @Override
//            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
//
//            }
//
//            @Override
//            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });
//    }
//
//
//    public void AddToTasks(MyTasks myTasks, String Key) {
//        SQLiteDatabase db = getReadableDatabase();
//        db.beginTransaction();
//
//        try {
//            ContentValues values = new ContentValues();
//            values.put(KEY_TASK_ID,/*Task ID Hhere */Key);
//            values.put(KEY_TASK_Finish_Time, myTasks.getFinishTime());
//            values.put(KEY_TASK_Start_Time, myTasks.getStartTime());
//            values.put(KEY_TASK_Deadline, myTasks.getDeadline());
//            values.put(KEY_TASK_Status, myTasks.getStatus());
//            values.put(KEY_TASK_CONTENT, myTasks.getTask());
//            values.put(KEY_TASK_user_Name, myTasks.getUserName());
//            values.put(KEY_TASK_User_ID, myTasks.getUserId());
//
//            // Notice how we haven't specified the primary key. SQLite auto increments the primary key column.
//            db.insertOrThrow(TABLE_TASKS, null, values);
//
//            db.setTransactionSuccessful();
//            //TRANSACTION COMPLETED
//            Log.d(TAG, "addToCart: TRANSACTION COMPLETED " + db.isOpen());
//        } catch (Exception e) {
//            Log.d(TAG, "Error while trying to add Order to database" + e.getMessage());
//        } finally {
//            db.endTransaction();
//            db.close();
//            Log.d(TAG, "addToCart: TRANSACTION COMPLETED " + db.isOpen());
//        }
//    }
//
//    /*
//        public void AddToGroupTasks(SingleGroupTask myTasks, String Key) {
//            Log.d(TAG, "AddToGroupTasks: Started ");
//
//            SQLiteDatabase db = getReadableDatabase();
//            db.beginTransaction();
//
//            try {
//                ContentValues values = new ContentValues();
//
//                values.put(KEY_GROUP_TASK_ID, Key);
//                values.put(KEY_TASK_CONTENT, myTasks.getTaskContent());
//                values.put(KEY_USER_UID, myTasks.getUserUID());
//
//                values.put(KEY_TASK_Deadline, myTasks.getDeadline());
//                values.put(KEY_TASK_Start_Time, myTasks.getStartTime());
//                values.put(KEY_TASK_Finish_Time, myTasks.getFinishTime());
//                values.put(KEY_USER_GROUP_ID, myTasks.getGroupId());
//                String Status = "";
//                if (myTasks.getStartTime() == 0) {
//                    Status = KEY_STATUS_PENDING;
//                } else if (myTasks.getStartTime() != 0) {
//                    if (myTasks.getFinishTime() == 0) {
//                        Status = KEY_STATUS_STARTED;
//                    } else {
//                        Status = CheckTimerStatus(myTasks.getDeadline(), myTasks.getFinishTime());
//                    }
//
//                }
//
//                values.put(KEY_TASK_Status, Status);
//
//
//                db.insertOrThrow(TABLE_GROUP_TASKS, null, values);
//
//                db.setTransactionSuccessful();
//                Log.d(TAG, "AddToGroupTasks: TRANSACTION COMPLETED " + db.isOpen());
//            } catch (Exception e) {
//                Log.d(TAG, "Error AddToGroupTasks database" + e.getMessage());
//            } finally {
//                db.endTransaction();
//                db.close();
//                Log.d(TAG, "AddToGroupTasks: TRANSACTION COMPLETED " + db.isOpen());
//            }
//        }
//    */
//    private boolean checkTaskExist(String key) {
//        SQLiteDatabase db = getReadableDatabase();
//        String query = String.format("SELECT * FROM " + TABLE_TASKS + " WHERE " + KEY_TASK_ID + "='%s';", key);
//        Cursor c = db.rawQuery(query, null);
//        Log.d(TAG, "checkTaskExist: " + c.getCount());
//        return c.getCount() > 0;
//    }
//
//    private boolean checkTaskGroupExist(String key, String userUid) {
//        SQLiteDatabase db = getReadableDatabase();
//        String query = String.format("SELECT * FROM " + TABLE_GROUP_TASKS + " WHERE " + KEY_GROUP_TASK_ID + "='%s'  AND " + KEY_TASK_User_UID +
//                "='%s' ;", key, userUid);
//        Cursor c = db.rawQuery(query, null);
//        Log.d(TAG, "checkTaskExist: " + c.getCount());
//        return c.getCount() > 0;
//    }
//
//    public List<MyTasks> getTasks() {
//
//        SQLiteDatabase db = getReadableDatabase();
//        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
//
//        String[] sqlSelect = {
//                KEY_TASK_ID,
//                KEY_TASK_Finish_Time,
//                KEY_TASK_Start_Time,
//                KEY_TASK_Deadline,
//                KEY_TASK_Status,
//                KEY_TASK_CONTENT,
//                KEY_TASK_user_Name,
//                KEY_TASK_User_ID
//        };
//
//        String sqlTable = TABLE_TASKS;
//
//
//        qb.setTables(sqlTable);
//        Cursor c = qb.query(db, sqlSelect, null, null, null, null, null);
//        final List<MyTasks> result = new ArrayList<>();
//        if (c.moveToFirst()) {
//            do {
////public MyTasks(String status, String task, String userName, String userId, long deadline, long finishTime, long startTime, long reciveTime) {
//                result.add(new MyTasks(
//                                c.getString(c.getColumnIndex(KEY_TASK_Status)),
//                                c.getString(c.getColumnIndex(KEY_TASK_CONTENT)),
//                                c.getString(c.getColumnIndex(KEY_TASK_user_Name)),
//                                c.getString(c.getColumnIndex(KEY_TASK_User_ID)),
//                                c.getLong(c.getColumnIndex(KEY_TASK_Deadline)),
//                                c.getLong(c.getColumnIndex(KEY_TASK_Finish_Time)),
//                                c.getLong(c.getColumnIndex(KEY_TASK_Start_Time))
//                        )
//
//                );
//            } while (c.moveToNext());
//        }
//
//        return result;
//    }
//
//    private void EditTask(MyTasks myTasks, String key) {
//        try {
//            SQLiteDatabase db = getReadableDatabase();
//            String Query2 = String.format("UPDATE " + TABLE_TASKS +
//                            " SET " + KEY_TASK_Start_Time + "= %s ," +
//                            KEY_TASK_Finish_Time + "= %s ," +
//                            KEY_TASK_User_ID + "= \"%s\" " +
//                            " WHERE " + KEY_TASK_ID + "=%s ;",
//
//                    myTasks.getStartTime(), myTasks.getFinishTime(), myTasks.getUserId(), key);
//
//            db.execSQL(Query2);
//            Log.d(TAG, "EditTask: " + Query2);
//        } catch (Exception e) {
//            Log.d(TAG, "EditTask: " + e.getMessage());
//        }
//    }
//
//    public void clear() {
//
//        SQLiteDatabase db = getReadableDatabase();
//        String query = String.format("DELETE FROM " + TABLE_TASKS);
//        db.execSQL(query);
//
//    }
//
//
//    public void ClearAllDatabase() {
//
//        SQLiteDatabase db = getReadableDatabase();
//        String query = String.format("DELETE FROM " + TABLE_TASKS);
//        db.execSQL(query);
//        String query2 = String.format("DELETE FROM " + TABLE_GROUP_TASKS);
//        db.execSQL(query2);
//        String query3 = String.format("DELETE FROM " + TABLE_STATICS);
//        db.execSQL(query3);
//
//
//    }
//
//    public List<MyTasks> getUserTasksIndividual(String userId) {
//
//        SQLiteDatabase db = this.getReadableDatabase();
//        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
//
//        String[] sqlSelect = {
//                KEY_TASK_ID,
//                KEY_TASK_Finish_Time,
//                KEY_TASK_Start_Time,
//                KEY_TASK_Deadline,
//                KEY_TASK_Status,
//                KEY_TASK_CONTENT,
//                KEY_TASK_user_Name,
//                KEY_TASK_User_ID
//        };
//
//        String sqlTable = TABLE_TASKS;
//
//
//        qb.setTables(sqlTable);
//        Cursor c = qb.query(db, sqlSelect, null, null, null, null, null);
//        final List<MyTasks> result = new ArrayList<>();
//        if (c.moveToFirst()) {
//            do {
//                if (c.getString(c.getColumnIndex(KEY_TASK_User_ID)).equals(userId)) {
//                    result.add(new MyTasks(
//                                    c.getString(c.getColumnIndex(KEY_TASK_Status)),
//                                    c.getString(c.getColumnIndex(KEY_TASK_CONTENT)),
//                                    c.getString(c.getColumnIndex(KEY_TASK_user_Name)),
//                                    c.getString(c.getColumnIndex(KEY_TASK_User_ID)),
//                                    c.getString(c.getColumnIndex(KEY_TASK_ID)),
//                                    c.getLong(c.getColumnIndex(KEY_TASK_Deadline)),
//                                    c.getLong(c.getColumnIndex(KEY_TASK_Finish_Time)),
//                                    c.getLong(c.getColumnIndex(KEY_TASK_Start_Time))
//                            )
//
//                    );
//                }
//            } while (c.moveToNext());
//        }
//
//        return result;
//    }
//
//
//}