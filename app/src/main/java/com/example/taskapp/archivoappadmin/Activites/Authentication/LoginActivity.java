package com.example.taskapp.archivoappadmin.Activites.Authentication;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.taskapp.archivoappadmin.Activites.HomeActivity;
import com.example.taskapp.archivoappadmin.Common.Common;
import com.example.taskapp.archivoappadmin.Models.Staff;
import com.example.taskapp.archivoappadmin.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import static com.example.taskapp.archivoappadmin.Common.Common.KEY_INTENT_EXTRA_MODEL;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_TABLE_STAFF;
import static com.example.taskapp.archivoappadmin.Common.Common.UpdateToken;

public class LoginActivity extends AppCompatActivity {


    private static final String TAG = "LoginActivity";
    private TextInputEditText et_password, et_email;
    private TextView tv_Welcome, tv_name;
    String ID;
    private Button btn_login;
    private FirebaseUser firebaseUser;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private Boolean isAdmin;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabaseReference;
    Staff user;
    private ValueEventListener userDataListener;
    private ChildEventListener childEventListener;

    @SuppressLint("SetTextI18n")
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mAuth = FirebaseAuth.getInstance();

        tv_Welcome = findViewById(R.id.tv_welcome);
        tv_name = findViewById(R.id.tv_name);
        if (getIntent() != null) {
            user = getIntent().getParcelableExtra(KEY_INTENT_EXTRA_MODEL);

            tv_name.setText(Common.CurrentStaff.getUserName());
            et_email = findViewById(R.id.et_email);

            et_email.setText(Common.CurrentStaff.getUserId());
            et_email.setEnabled(false);
        }


        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = mFirebaseDatabase.getReference(KEY_TABLE_STAFF);


        et_password = findViewById(R.id.et_password);

        btn_login = findViewById(R.id.btn_Login);
        final ProgressDialog mDialog = new ProgressDialog(LoginActivity.this);
        mDialog.setMessage("Please Wait  ...");

        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                firebaseUser = firebaseAuth.getCurrentUser();
                if (firebaseUser != null) {
                    mDatabaseReference.addListenerForSingleValueEvent(userDataListener);
                    mDialog.dismiss();
                } else {
                    // User is signed out
                    mDialog.dismiss();
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };

        userDataListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                try {
                    Common.CurrentStaff = dataSnapshot.child(firebaseUser.getUid()).getValue(Staff.class);
                    UpdateToken(FirebaseInstanceId.getInstance().getToken());
                    StartApp();
                } catch (Exception e) {
                    System.out.println(TAG + e.toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };


        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String email = Common.CurrentStaff.getEmail();
                    checkValidation(et_password);
                    mDialog.show();
                    mAuth.signInWithEmailAndPassword(email, et_password.getText().toString())
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (!task.isSuccessful()) {
                                        mDialog.dismiss();
                                        Log.d(TAG, "onComplete: 124 " + task.getException().toString());
                                    } else {
                                        firebaseUser = mAuth.getCurrentUser();
                                        mDialog.dismiss();
                                        DatabaseReference user_info = mDatabaseReference;
                                        user_info.addValueEventListener(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                Common.CurrentStaff = dataSnapshot.child(firebaseUser.getUid()).getValue(Staff.class);
                                                mDialog.dismiss();
                                                UpdateToken(FirebaseInstanceId.getInstance().getToken());
                                                StartApp();
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                                mDialog.dismiss();

                                            }
                                        });

                                    }
                                    mDialog.dismiss();
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            mDialog.dismiss();
                        }
                    });
                } catch (IllegalArgumentException e) {
                    mDialog.dismiss();
                    Toast.makeText(LoginActivity.this, "Password Rqured", Toast.LENGTH_SHORT).show();
                }


            }
        });
    }

    private void checkValidation(EditText editText) {
        String id = editText.getText().toString();
        if (id.isEmpty() || id.equals("")) {
            et_password.setError(getString(R.string.input_error_ID));
            et_password.requestFocus();
            return;
        }
    }

    private void StartApp() {
        Intent i = new Intent(LoginActivity.this, HomeActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthStateListener);
    }


}
