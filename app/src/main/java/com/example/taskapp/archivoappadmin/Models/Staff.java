package com.example.taskapp.archivoappadmin.Models;

import android.os.Parcel;
import android.os.Parcelable;

public class Staff implements Parcelable {

    private String email, password, userId, userName, uid;
    private int rank;

    public Staff() {
    }

    protected Staff(Parcel in) {
        email = in.readString();
        password = in.readString();
        userId = in.readString();
        userName = in.readString();
        uid = in.readString();
        rank = in.readInt();
    }

    public static final Creator<Staff> CREATOR = new Creator<Staff>() {
        @Override
        public Staff createFromParcel(Parcel in) {
            return new Staff(in);
        }

        @Override
        public Staff[] newArray(int size) {
            return new Staff[size];
        }
    };

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    @Override
    public String toString() {
        return "Staff{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", uid='" + uid + '\'' +
                ", rank=" + rank +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(email);
        dest.writeString(password);
        dest.writeString(userId);
        dest.writeString(userName);
        dest.writeString(uid);
        dest.writeInt(rank);
    }
}
