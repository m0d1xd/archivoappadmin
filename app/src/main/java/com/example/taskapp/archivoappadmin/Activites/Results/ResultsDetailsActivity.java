package com.example.taskapp.archivoappadmin.Activites.Results;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.example.taskapp.archivoappadmin.Models.MyTasks;
import com.example.taskapp.archivoappadmin.Models.User;
import com.example.taskapp.archivoappadmin.R;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Date;

import static com.example.taskapp.archivoappadmin.Common.Common.Difference;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_INTENT_EXTRA_MODEL;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_STATUS_STARTED;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_TABLE_TASK;

public class ResultsDetailsActivity extends AppCompatActivity {

    private static final String TAG = "TaskDetailsActivity";
    private MyTasks myTasks;
    private TextView tv_Task_details, tv_Status, tv_timer, tv_name;
    private Button btn_trace;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private User user;
    String TaskID, UserID;
    private MapView mMapView;
    Double lat, lon;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results_details);
        tv_Task_details = findViewById(R.id.tvtask);
        tv_Status = findViewById(R.id.tvstatus);
        btn_trace = findViewById(R.id.btn_Trace);
        tv_timer = findViewById(R.id.tvtimer);
        tv_name = findViewById(R.id.tv_name);

        mMapView = findViewById(R.id.mapView);

        MapsInitializer.initialize(ResultsDetailsActivity.this);

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference(KEY_TABLE_TASK);

        if (getIntent() != null) {
            myTasks = getIntent().getParcelableExtra(KEY_INTENT_EXTRA_MODEL);
            if (myTasks.getStatus().equals(KEY_STATUS_STARTED)) {
                mMapView.setVisibility(View.VISIBLE);
                CheckIfCoordinateExists(myTasks.getUserUid());
            }


            final Thread t = new Thread() {
                @Override
                public void run() {
                    try {
                        while (!isInterrupted()) {
                            Thread.sleep(1000);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    updateTextView();
                                }

                                private void updateTextView() {
                                    Date d2 = new Date(System.currentTimeMillis());
                                    if (myTasks.getStartTime() != 0) {
                                        tv_timer.setText(Difference(new Date(myTasks.getStartTime()), d2));

                                    }
                                }
                            });
                        }
                    } catch (InterruptedException e) {
                    }
                }
            };


            // tv_name.setText(user.getUserName() + getString(R.string.tv_assiend_tasksTo));

            tv_Status.setText(myTasks.getStatus());
            tv_Task_details.setText(myTasks.getTask());

            if (myTasks.getFinishTime() == 0) {
                t.start();
            }


            btn_trace.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (myTasks.getStatus().equals(KEY_STATUS_STARTED)) {
                        if (lat != null && lon != null) {
                            showMap(lat, lon);
                        }
                    } else {

                    }

                }
            });
        }

    }

    private void showMap(final double lat, final double lng) {

        final Dialog dialog = new Dialog(this);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        /////make map clear
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FIRST_SUB_WINDOW);

        dialog.setContentView(R.layout.dialogmap);////your custom content

        MapView mMapView = (MapView) dialog.findViewById(R.id.mapView);
        MapsInitializer.initialize(this);

        mMapView.onCreate(dialog.onSaveInstanceState());
        mMapView.onResume();


        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(final GoogleMap googleMap) {

                LatLng posisiabsen = new LatLng(lat, lng); ////your lat lng
               // googleMap.addMarker(new MarkerOptions().position(posisiabsen).title("Yout title"));

                googleMap.moveCamera(CameraUpdateFactory.newLatLng(posisiabsen));
                googleMap.animateCamera(CameraUpdateFactory.zoomTo(13));

            }
        });


        dialog.show();
    }

    private void CheckIfCoordinateExists(final String uid) {

        DatabaseReference ref = firebaseDatabase.getReference("StartedTasks").child(uid);
        ref.addValueEventListener(new ValueEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                try {
                    lat = Double.parseDouble("" + dataSnapshot.child("l").child("0").getValue());
                    lon = Double.parseDouble("" + dataSnapshot.child("l").child("1").getValue());
                    Log.d(TAG, "onDataChange: lat " + lat + " lon = " + lon);
                    final LatLng l = new LatLng(lat, lon);
                } catch (Exception e) {
                    Log.d(TAG, "onDataChange: " + e.getMessage());
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d(TAG, "onCancelled: " + databaseError.getDetails());
            }
        });


    }


}

