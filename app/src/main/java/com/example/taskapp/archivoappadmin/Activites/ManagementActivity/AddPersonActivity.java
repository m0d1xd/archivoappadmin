package com.example.taskapp.archivoappadmin.Activites.ManagementActivity;

import android.app.ProgressDialog;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.taskapp.archivoappadmin.Adapters.SpinnerArrayAdapter;
import com.example.taskapp.archivoappadmin.Models.Groups;
import com.example.taskapp.archivoappadmin.Models.User;
import com.example.taskapp.archivoappadmin.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static com.example.taskapp.archivoappadmin.Common.Common.GroupList;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_QUERY_FILTER_USER_ID;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_REGEX_1;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_REGEX_2;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_TABLE_USERS;

public class AddPersonActivity extends AppCompatActivity {
    final String TAG = "AddPersonActivity";

    private ValueEventListener dataValueEventListener;

    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabaseReference;
    private TextInputEditText edt_personname, edt_personid, edt_password;
    private TextView tv_back;
    private Button btn_create;
    boolean ExistId = false;
    private Spinner sp_groups;
    private ProgressDialog progressDialog;
    private ArrayList<Groups> groups = new ArrayList<>();
    private String groupId = "00";
    private String randomID;
    private SpinnerArrayAdapter sp_adapter;
    Query query;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_person);

        //init firebase
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = mFirebaseDatabase.getReference(KEY_TABLE_USERS);


        randomID = GenerateID();


        //init Spinner Groups
        sp_groups = findViewById(R.id.sp_groups);

        sp_adapter = new SpinnerArrayAdapter(this, GroupList);
        sp_groups.setAdapter(sp_adapter);


        sp_groups.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    groupId = GroupList.get(position).getGroupId();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                groupId = "00";
            }
        });

        //init Random User ID
        edt_personid = (TextInputEditText) findViewById(R.id.et_person_id);

        edt_personid.setText(randomID);

        Toast.makeText(AddPersonActivity.this, "Auto ID has been Automatically generated", Toast.LENGTH_SHORT).show();

        //init back tv
        tv_back = (TextView) findViewById(R.id.tv_back);

        tv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Back is pressed", Toast.LENGTH_SHORT).show();

            }
        });

        //init Person name
        edt_personname = (TextInputEditText) findViewById(R.id.et_person_name);


        //init password field
        edt_password = (TextInputEditText) findViewById(R.id.et_person_password);

        //init if is admin
        btn_create = (Button) findViewById(R.id.btncreate);


        //TODO FIX User Creation
        btn_create.setOnClickListener(new View.OnClickListener() {
            @Override
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            public void onClick(View v) {
                String name, email, password, id, group;
                name = edt_personname.getText().toString();
                //       email = edt_email.getText().toString();
                password = edt_password.getText().toString();
                id = edt_personid.getText().toString();
                if (!groupId.equals("00")) {
                    group = groupId;
                    if (name.equals("") || name.isEmpty()) {
                        edt_personname.setError(getString(R.string.input_error_name));
                        edt_personname.requestFocus();
                    } else {
                        if (checkIdFormat(id)) {
                            edt_personid.setError(getString(R.string.input_error_Format_ID));
                            edt_personid.requestFocus();
                        } else {
                            if (checkExistID(id)) {
                                edt_personid.setError(getString(R.string.input_error_userID_Exist));
                                edt_personid.requestFocus();

                            } else {
                                User user = new User(name, id, "", "", password, group, false, "");
                                CreateUser(user);
                            }
                        }

                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Plese Chose A group", Toast.LENGTH_SHORT).show();
                }

            }

        });

        ExistId = false;
    }


    private boolean checkIdFormat(String str) {
        boolean check = false;
        if (str.matches(KEY_REGEX_1) || str.matches(KEY_REGEX_2)) {
            check = true;
        }
        return !check;
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void checkValidation(User user) {

        String name, id, password;
        //Checking Name Is not Empty
        name = user.getUserName();

        id = user.getUserId();
        password = user.getPassword();

        if (name.equals("") || name.isEmpty()) {
            edt_personname.setError(getString(R.string.input_error_name));
            edt_personname.requestFocus();
            return;
        } else if (checkIdFormat(id)) {
            edt_personid.setError(getString(R.string.input_error_Format_ID));
            edt_personid.requestFocus();
            return;
        } else if (checkExistID(id)) {
            return;
        } else if (password.equals("") || password.length() < 6) {
            if (password.equals("")) {
                edt_password.setError(getString(R.string.input_error_password));
            } else {
                edt_password.setError(getString(R.string.input_error_password_length));
            }
            edt_password.requestFocus();

            return;
        }

    }


    private String GenerateID() {
        StringBuilder id = new StringBuilder();
        id.append(String.valueOf((int) (Math.random() * 100)))
                .append(".")
                .append(String.valueOf((int) (Math.random() * 1000)))
                .append(".")
                .append(String.valueOf((int) (Math.random() * 1000)))
                .append("-")
                .append(String.valueOf((int) (Math.random() * 10)));
        return id.toString();
    }


    //Method to check if id exists
    private Boolean checkExistID(String ID) {
        final Query query = mDatabaseReference.orderByChild(KEY_QUERY_FILTER_USER_ID).equalTo(ID);
        final Boolean[] t = {false};
        dataValueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    t[0] = dataSnapshot.getChildrenCount() > 0;
                    Log.d(TAG, "checkExistID onDataChange: dataSnapshot" + dataSnapshot.getChildrenCount());
                    query.removeEventListener(this);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

        query.addValueEventListener(dataValueEventListener);


        return t[0];
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void CreateUser(User user) {
        DatabaseReference db = mDatabaseReference;
        db.push().setValue(user)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(AddPersonActivity.this, "User Was added Successfully ", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(AddPersonActivity.this, "Error Adding User", Toast.LENGTH_SHORT).show();

                        }
                    }
                });
        ExistId = false;
    }


}
