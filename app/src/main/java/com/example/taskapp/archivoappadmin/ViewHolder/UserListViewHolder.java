package com.example.taskapp.archivoappadmin.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.taskapp.archivoappadmin.Interface.ItemClickListener;
import com.example.taskapp.archivoappadmin.R;

public class UserListViewHolder extends RecyclerView.ViewHolder
        implements View.OnClickListener {

    public TextView custom_tv_userName, custom_tv_userId, custom_tv_personGroupID;
    public ImageView custom_iv_userImg;
    private ItemClickListener itemClickListener;

    public UserListViewHolder(View itemView) {
        super(itemView);
        custom_tv_userId = itemView.findViewById(R.id.custom_tv_userId);
        custom_tv_userName = itemView.findViewById(R.id.custom_tv_userName);
        custom_tv_personGroupID = itemView.findViewById(R.id.custom_tv_personGroupID);
        custom_iv_userImg = itemView.findViewById(R.id.custom_iv_userImg);
        itemView.setOnClickListener(this);
    }


    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view, getAdapterPosition(), false);
    }
}
