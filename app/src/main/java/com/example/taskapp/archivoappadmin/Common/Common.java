package com.example.taskapp.archivoappadmin.Common;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.example.taskapp.archivoappadmin.Models.Groups;
import com.example.taskapp.archivoappadmin.Models.NotificationModel.Token;
import com.example.taskapp.archivoappadmin.Models.Staff;

import com.example.taskapp.archivoappadmin.Remote.APIServices;
import com.example.taskapp.archivoappadmin.Remote.RetrofitClient;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Common {

    public static Staff CurrentStaff;

    public static ArrayList<Groups> GroupList;

    public static String getGroupName(String groupId) {
        String groupName = "";
        if (GroupList.size() > 0) {
            for (Groups gr : GroupList) {
                if (groupId.equals(gr.getGroupId())) {
                    groupName = gr.getGroupName();
                }
            }
        }
        return groupName;
    }

    final public static String KEY_STATUS_DEFAULT = "ALL";
    final public static String KEY_STATUS_COMPLETED_ON_TIME = "Completed on time";
    final public static String KEY_STATUS_LATE_COMPLETE = "Late completed";
    final public static String KEY_STATUS_STARTED = "Started";
    final public static String KEY_STATUS_START = "Start";
    final public static String KEY_STATUS_PENDING = "Pending";
    final public static String KEY_STATUS_FINISH = "Finish";
    final public static String KEY_INTENT_EXTRA_MODEL = "model";

    final public static String KEY_TABLE_STAFF = "Staff";
    final public static String KEY_TABLE_USERS = "Users";
    final public static String KEY_TABLE_GROUPS = "Groups";
    final public static String KEY_TABLE_TASK = "TableTasks";
    final public static String KEY_TABLE_GROUPS_TASK = "GroupTasks";
    final public static String KEY_TABLE_TOKENS = "Tokens";


    final public static String KEY_INTENT_EXTRA_USER = "user_model";

    //REGEX for ID Format
    final public static String KEY_REGEX_1 = "\\d{2}.\\d{3}.\\d{3}-[kK]";
    final public static String KEY_REGEX_2 = "\\d{2}.\\d{3}.\\d{3}-\\d";


    final public static String KEY_QUERY_FILTER_ADMINUID = "adminUid";
    final public static String KEY_QUERY_FILTER_ADMINID = "adminId";
    final public static String KEY_QUERY_FILTER_USER_ID = "userId";
    final public static String KEY_QUERY_FILTER_USER_UID = "userUid";
    final public static String KEY_QUERY_FILTER_USER_NAME = "userName";
    final public static String KEY_QUERY_FILTER_STATUS = "status";
    final public static String KEY_QUERY_FILTER_GROUP_ID = "groupId";


    public static final String BASE_URL = "https://fcm.googleapis.com/";

    //Checking Internet Connection
    public static Boolean IsConnectedToInternet(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static void UpdateToken(String token) {
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        DatabaseReference tokens = db.getReference(KEY_TABLE_TOKENS);
        Token tokenData = new Token(token, "true");
        tokens.child(Common.CurrentStaff.getUid()).setValue(tokenData);
    }

    public static String getDate(long milliSeconds) {

        String dateFormat = "dd/MM/yyyy hh:mm:ss";
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public static APIServices getFCMService() {
        return RetrofitClient.getClient(BASE_URL).create(APIServices.class);
    }

    public static String Difference(Date startDate, Date endDate) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        String date;
        if (elapsedDays == 0) {
            date = String.valueOf(elapsedHours) + ":" + String.valueOf(elapsedMinutes) + ":" + String.valueOf(elapsedSeconds);
        } else {
            date = String.valueOf(elapsedDays) +
                    " d " + String.valueOf(elapsedHours) + ":" + String.valueOf(elapsedMinutes) + ":" + String.valueOf(elapsedSeconds);
        }

        return date;
    }


    public static boolean checkIdFormat(String str) {
        boolean check = false;
        if (str.matches(KEY_REGEX_1) || str.matches(KEY_REGEX_2)) {
            check = true;
        }

        return check;
    }

    public static String CheckTimerStatus(long DurationTime, long finishTime) {
        String Delivery_status;

        Calendar calendar = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        calendar.setTimeInMillis(DurationTime);
        calendar2.setTimeInMillis(finishTime);


        if (calendar2.before(calendar)) {
            Delivery_status = KEY_STATUS_COMPLETED_ON_TIME;
        } else {
            Delivery_status = KEY_STATUS_LATE_COMPLETE;
        }


        return Delivery_status;
    }

}
