package com.example.taskapp.archivoappadmin.Activites.CheckingActivities;

import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.taskapp.archivoappadmin.Common.Common;
import com.example.taskapp.archivoappadmin.Interface.ItemClickListener;
import com.example.taskapp.archivoappadmin.Models.MyTasks;
import com.example.taskapp.archivoappadmin.R;
import com.example.taskapp.archivoappadmin.ViewHolder.MyTasksViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import static com.example.taskapp.archivoappadmin.Common.Common.KEY_QUERY_FILTER_USER_ID;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_REGEX_1;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_REGEX_2;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_TABLE_TASK;

public class CheckTaskByIDActivity extends AppCompatActivity {

    private static final String TAG = "CheckByID";

    private TextInputEditText edt_user_id;

    private TextView tv_go;

    private RecyclerView.LayoutManager layoutManager;

    private RecyclerView rv_list_tasks;

    FirebaseRecyclerAdapter<MyTasks, MyTasksViewHolder> adapter;

    private FirebaseDatabase firebaseDatabase;

    private DatabaseReference databaseReference;

    private Button btn_search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_task_by_id);

        //init Firebase
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference(KEY_TABLE_TASK);
        //init Edittext

        edt_user_id = findViewById(R.id.et_person_id);

        rv_list_tasks = findViewById(R.id.rv_list_tasks);

        rv_list_tasks.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rv_list_tasks.setLayoutManager(layoutManager);

        //init Search Button
        btn_search = findViewById(R.id.btn_searchID);
        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkID(edt_user_id)) {
                    if (checkIdFormat(edt_user_id)) {
                        DoSearch(edt_user_id.getText().toString());
                    } else {
                        edt_user_id.setError(getString(R.string.input_error_Format_ID));
                        edt_user_id.requestFocus();
                    }
                } else {
                    edt_user_id.setError(getString(R.string.input_error_nameID));
                    edt_user_id.requestFocus();
                }

            }
        });
    }

    private Boolean checkID(EditText editText) {
        Boolean check = true;
        String srarched_id = editText.getText().toString();
        if (srarched_id.isEmpty()) {
            check = false;
        }

        return check;
    }


    private boolean checkIdFormat(EditText editText) {
        boolean check = false;
        String str = editText.getText().toString();
        if (str.matches(KEY_REGEX_1) || str.matches(KEY_REGEX_2)) {
            check = true;
        }

        return check;
    }


    private void DoSearch(String srarched_id) {

        Query query = databaseReference.orderByChild(KEY_QUERY_FILTER_USER_ID).equalTo(srarched_id);
        FirebaseRecyclerOptions<MyTasks> MyTaskListFirebaseRecyclerOptions = new FirebaseRecyclerOptions.Builder<MyTasks>()
                .setQuery(query, MyTasks.class)
                .build();

        adapter = new FirebaseRecyclerAdapter<MyTasks, MyTasksViewHolder>(MyTaskListFirebaseRecyclerOptions) {
            @Override
            protected void onBindViewHolder(@NonNull MyTasksViewHolder holder, int position, @NonNull MyTasks model) {
                holder.tv_username.setText((model.getUserName()));
                holder.tv_tasksummary.setText(model.getTask());
                holder.tv_taskstatus.setText(model.getStatus());
                holder.tv_time.setText(Common.getDate(model.getDeadline()));
                holder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {

                    }
                });
                Log.d("Test", "onBindViewHolder: " + model.toString());
            }

            @NonNull
            @Override
            public MyTasksViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.rv_task_list_layout, parent, false);

                return new MyTasksViewHolder(itemView);


            }
        };


        adapter.startListening();
        rv_list_tasks.setAdapter(adapter);

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (adapter != null)
            adapter.stopListening();
    }
}
