package com.example.taskapp.archivoappadmin.Activites.ManagementActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.taskapp.archivoappadmin.Adapters.SpinnerArrayAdapter;
import com.example.taskapp.archivoappadmin.Common.Common;
import com.example.taskapp.archivoappadmin.Interface.ItemClickListener;
import com.example.taskapp.archivoappadmin.Models.Groups;
import com.example.taskapp.archivoappadmin.Models.User;
import com.example.taskapp.archivoappadmin.R;
import com.example.taskapp.archivoappadmin.ViewHolder.AdminUserListViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static com.example.taskapp.archivoappadmin.Common.Common.GroupList;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_INTENT_EXTRA_USER;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_QUERY_FILTER_GROUP_ID;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_TABLE_USERS;

public class PersonListActivity extends AppCompatActivity {
    private static final String TAG = "AdminPersonListActivity";
    private TextView tv_add_person, tv_give_task_to_group;

    private RecyclerView rv_listOfUsers;
    private RecyclerView.LayoutManager layoutManager;
    private Spinner sp_groups;

    private ArrayList<Groups> groups = new ArrayList<>();
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private FirebaseRecyclerAdapter<User, AdminUserListViewHolder> adapter;
    private FirebaseRecyclerOptions<User> MyTaskListFirebaseRecyclerOptions;
    private ValueEventListener groupsValueEventListener;
    String groupId;
    SpinnerArrayAdapter sp_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_list);


        tv_add_person = findViewById(R.id.tv_addPerson);
        tv_give_task_to_group = findViewById(R.id.tv_task_To_Group);

        rv_listOfUsers = findViewById(R.id.rv_users);


        layoutManager = new GridLayoutManager(this, 3);
        //  layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv_listOfUsers.setLayoutManager(layoutManager);

        //Init Firebase
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference(KEY_TABLE_USERS);

        //TODO Create firebase filter depending on group id
        //Create Spinner Adapter with implemented filter adapter
        //Create Fetch method to get groups
        // fetchGroups();
        groups.clear();

        if (Common.GroupList == null) {
            Common.GroupList = new ArrayList<>();
        }

        groupsValueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    Groups group = ds.getValue(Groups.class);
                    Common.GroupList.add(group);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };


        sp_groups = findViewById(R.id.sp_filter_group);


        sp_adapter = new SpinnerArrayAdapter(this, GroupList);

        sp_groups.setAdapter(sp_adapter);
        sp_groups.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SelectedGroup(Common.GroupList.get(position));
                groupId = (Common.GroupList.get(position).getGroupId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Query query = databaseReference;

        MyTaskListFirebaseRecyclerOptions = new FirebaseRecyclerOptions.Builder<User>()
                .setQuery(query, User.class)
                .build();

        adapter = new FirebaseRecyclerAdapter<User, AdminUserListViewHolder>(MyTaskListFirebaseRecyclerOptions) {
            @Override
            protected void onBindViewHolder(@NonNull AdminUserListViewHolder holder, int position, @NonNull final User model) {
                holder.tv_user.setText(model.getUserName());

                if (model.getOnline()) {
                    holder.iv_active.setImageResource(R.drawable.circle_green);
                } else {
                    holder.iv_active.setImageResource(R.drawable.circle_red);
                }
                if (!model.getImageUrl().equals("")) {
                    Picasso.with(getApplicationContext())
                            .load(model.getImageUrl())
                            .into(holder.iv_profile);
                } else {


                }
                Log.d(TAG, "onBindViewHolder: " + model.getUserId());
                holder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        Intent i = new Intent(PersonListActivity.this, AddTaskToUserActivity.class);
                        i.putExtra(KEY_INTENT_EXTRA_USER, model);
                        startActivity(i);
                    }
                });
            }

            @NonNull
            @Override
            public AdminUserListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.adapter_listview_users, parent, false);


                return new AdminUserListViewHolder(itemView);
            }
        };

        adapter.startListening();
        adapter.notifyDataSetChanged();

        rv_listOfUsers.setAdapter(adapter);

        tv_add_person.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PersonListActivity.this, AddPersonActivity.class);
                startActivity(i);
            }
        });

        tv_give_task_to_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!groupId.equals("00")) {
                    Intent i = new Intent(PersonListActivity.this, AddTaskToGroup.class);
                    i.putExtra(KEY_QUERY_FILTER_GROUP_ID, groupId);
                    startActivity(i);
                }

            }
        });
    }

    private void SelectedGroup(Groups gr) {

        Query query;
        if (!gr.getGroupId().equals("00")) {
            query = databaseReference.orderByChild("groupId").equalTo(gr.getGroupId());
        } else {
            query = databaseReference;
        }
        MyTaskListFirebaseRecyclerOptions = new FirebaseRecyclerOptions.Builder<User>()
                .setQuery(query, User.class)
                .build();

        adapter = new FirebaseRecyclerAdapter<User, AdminUserListViewHolder>(MyTaskListFirebaseRecyclerOptions) {
            @Override
            protected void onBindViewHolder(@NonNull AdminUserListViewHolder holder, int position, @NonNull final User model) {
                holder.tv_user.setText(model.getUserName());

                try {
                    if (model.getOnline()) {
                        holder.iv_active.setImageResource(R.drawable.circle_green);
                    } else {
                        holder.iv_active.setImageResource(R.drawable.circle_red);
                    }
                } catch (Exception e) {
                }
                Log.d(TAG, "onBindViewHolder: " + model.getUserId());
                holder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        Intent i = new Intent(PersonListActivity.this, AddTaskToUserActivity.class);
                        i.putExtra(KEY_INTENT_EXTRA_USER, model);
                        startActivity(i);
                    }
                });
            }

            @NonNull
            @Override
            public AdminUserListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.adapter_listview_users, parent, false);


                return new AdminUserListViewHolder(itemView);
            }
        };

        adapter.startListening();
        adapter.notifyDataSetChanged();

        rv_listOfUsers.setAdapter(adapter);

    }


}
