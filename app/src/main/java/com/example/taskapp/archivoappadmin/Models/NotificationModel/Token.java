package com.example.taskapp.archivoappadmin.Models.NotificationModel;

import com.google.gson.annotations.SerializedName;

public class Token {

    @SerializedName("token")
    private String token;

    @SerializedName("isServerToken")
    private String isServerToken;

    public Token() {
    }

    public Token(String token, String isServerToken) {
        this.token = token;
        this.isServerToken = isServerToken;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getServerToken() {
        return isServerToken;
    }

    public void setServerToken(String serverToken) {
        isServerToken = serverToken;
    }
}
