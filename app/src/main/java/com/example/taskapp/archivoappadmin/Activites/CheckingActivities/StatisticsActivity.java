package com.example.taskapp.archivoappadmin.Activites.CheckingActivities;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.taskapp.archivoappadmin.Adapters.StatisticsListAdapter;
import com.example.taskapp.archivoappadmin.Database.DatabaseHelper;
import com.example.taskapp.archivoappadmin.Models.GroupsTaskTime;
import com.example.taskapp.archivoappadmin.Models.GroupsTasks;
import com.example.taskapp.archivoappadmin.Models.MyTasks;
import com.example.taskapp.archivoappadmin.Models.Statistics;
import com.example.taskapp.archivoappadmin.Models.User;
import com.example.taskapp.archivoappadmin.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.example.taskapp.archivoappadmin.Common.Common.KEY_TABLE_GROUPS_TASK;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_TABLE_TASK;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_TABLE_USERS;

public class StatisticsActivity extends AppCompatActivity {


    private static final String TAG = "StatisticsActivity";

    private FirebaseDatabase firebaseDatabase;


    private RecyclerView.LayoutManager layoutManager;

    Spinner spinner;

    ArrayList<Statistics> statistics;
    List<User> users;

    RecyclerView recyclerView;
    StatisticsListAdapter adapter;

    //STATISTICS CLASS STILL Having problems
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        firebaseDatabase = FirebaseDatabase.getInstance();

        recyclerView = findViewById(R.id.statistics_filter);

        final DatabaseHelper mdb = new DatabaseHelper(this);

        DatabaseReference db_users = firebaseDatabase.getReference(KEY_TABLE_USERS);
        db_users.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    Log.d(TAG, "onDataChange: Users ");
                    User user = ds.getValue(User.class);
                    mdb.addOrUpdateUser(user);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        DatabaseReference db_Tasks = firebaseDatabase.getReference(KEY_TABLE_TASK);

        db_Tasks.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    MyTasks myTasks = ds.getValue(MyTasks.class);
                    Log.d(TAG, "onDataChange: Tasks");
                    mdb.addOrUpdateTasks(myTasks, ds.getKey());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        DatabaseReference db_Group = firebaseDatabase.getReference(KEY_TABLE_GROUPS_TASK);

        db_Group.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.d(TAG, "onDataChange: dataSnapshotCount " + dataSnapshot.getChildrenCount());
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    GroupsTasks groupsTasks = ds.getValue(GroupsTasks.class);
                    try {
                        if (groupsTasks != null) {
                            mdb.AddGroupTask(groupsTasks, ds.getKey());
                            HashMap<String, GroupsTaskTime> hashMap = groupsTasks.getTimeTable();
                            Log.d(TAG, "onDataChange: groupsTasks.getTimeTable().size() " + hashMap.size());
                        }
                    } catch (Exception e) {
                        Log.d(TAG, "onDataChange: hashMap ");
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);


        spinner = findViewById(R.id.sp_filter);
        final List<String> list;
        list = new ArrayList<>();
        list.add("ALL");
        list.add("Users More than 50% completed tasks");
        list.add("Users Less than 50% completed tasks");
        list.add("Users with 0 finished Tasks");

        ArrayAdapter<String> filteredAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, list);
        try {
            spinner.setAdapter(filteredAdapter);

            final DatabaseHelper databaseHelper = new DatabaseHelper(this);
            databaseHelper.UpdateStatistics();

            statistics = databaseHelper.getAllStatistics();

            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    adapter = new StatisticsListAdapter(getApplicationContext(), statistics, String.valueOf(spinner.getItemAtPosition(position)), databaseHelper);
                    recyclerView.setAdapter(adapter);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    adapter = new StatisticsListAdapter(getApplicationContext(), statistics, "ALL", databaseHelper);
                }
            });

        } catch (Exception e) {
            Log.d(TAG, "onCreate: " + e.getMessage());
        }

        Log.d(TAG, "onClick: size ");
    }

    @Override
    protected void onStart() {
        super.onStart();
        statistics = new DatabaseHelper(this).getAllStatistics();
    }

}
