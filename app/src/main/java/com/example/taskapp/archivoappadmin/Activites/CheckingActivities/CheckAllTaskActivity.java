package com.example.taskapp.archivoappadmin.Activites.CheckingActivities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.taskapp.archivoappadmin.Activites.Results.ResultsDetailsActivity;
import com.example.taskapp.archivoappadmin.Common.Common;
import com.example.taskapp.archivoappadmin.Interface.ItemClickListener;
import com.example.taskapp.archivoappadmin.Models.MyTasks;
import com.example.taskapp.archivoappadmin.R;
import com.example.taskapp.archivoappadmin.ViewHolder.MyTasksViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import static com.example.taskapp.archivoappadmin.Common.Common.CurrentStaff;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_INTENT_EXTRA_MODEL;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_QUERY_FILTER_ADMINUID;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_TABLE_TASK;

public class CheckAllTaskActivity extends AppCompatActivity {

    private RecyclerView.LayoutManager layoutManager;

    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference mDatabaseReference;
    private FirebaseRecyclerAdapter<MyTasks, MyTasksViewHolder> adapter;
    private RecyclerView rv_TaskList;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_all_task);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(true);
        progressDialog.show();

        rv_TaskList = findViewById(R.id.rv_TaskList);
        rv_TaskList.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rv_TaskList.setLayoutManager(layoutManager);

        //init firebase
        firebaseDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = firebaseDatabase.getReference(KEY_TABLE_TASK);

        //Search Query to find the users belonging to admin


        Query query = mDatabaseReference;


        FirebaseRecyclerOptions<MyTasks> MyTaskListFirebaseRecyclerOptions = new FirebaseRecyclerOptions.Builder<MyTasks>()
                .setQuery(query, MyTasks.class)
                .build();

        adapter = new FirebaseRecyclerAdapter<MyTasks, MyTasksViewHolder>(MyTaskListFirebaseRecyclerOptions) {
            @Override
            protected void onBindViewHolder(@NonNull final MyTasksViewHolder holder, int position, @NonNull final MyTasks model) {
                holder.tv_username.setText((model.getUserName()));
                holder.tv_tasksummary.setText(model.getTask());
                holder.tv_taskstatus.setText(model.getStatus());
                holder.tv_time.setText(Common.getDate(model.getDeadline()));

                holder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        //TODO Fix the tsak view here
                        Intent i = new Intent(CheckAllTaskActivity.this, ResultsDetailsActivity.class);
                        //   Toast.makeText(CheckAllTaskActivity.this, "Task clicked", Toast.LENGTH_SHORT).show();
                        i.putExtra(KEY_INTENT_EXTRA_MODEL, model);
                        startActivity(i);

                    }
                });
                Log.d("Test", "onBindViewHolder: " + model.toString());
            }


            @NonNull
            @Override
            public MyTasksViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.rv_task_list_layout, parent, false);
                progressDialog.dismiss();
                return new MyTasksViewHolder(itemView);
            }
        };

        progressDialog.dismiss();
        adapter.startListening();
        adapter.notifyDataSetChanged();
        rv_TaskList.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (adapter != null)
            adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}
