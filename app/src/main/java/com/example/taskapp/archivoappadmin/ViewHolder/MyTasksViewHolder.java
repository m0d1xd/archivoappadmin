package com.example.taskapp.archivoappadmin.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.taskapp.archivoappadmin.Interface.ItemClickListener;
import com.example.taskapp.archivoappadmin.R;


public class MyTasksViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView tv_username, tv_taskstatus, tv_tasksummary, tv_time;

    public ItemClickListener itemClickListener;

    public MyTasksViewHolder(View itemView) {
        super(itemView);
        tv_username = itemView.findViewById(R.id.tvusrname);
        tv_tasksummary = itemView.findViewById(R.id.tvtask);
        tv_taskstatus = itemView.findViewById(R.id.tvtaskstatus);
        tv_time = itemView.findViewById(R.id.tvtime);
        itemView.setOnClickListener(this);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view, getAdapterPosition(), false);
    }
}
