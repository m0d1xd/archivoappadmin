package com.example.taskapp.archivoappadmin.Models.NotificationModel;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FCCResponse {

    @SerializedName("multicast_id")
    public long multicast_id;

    @SerializedName("success")
    public int success;

    @SerializedName("failure")
    public int failure;


    @SerializedName("canonical_ids")
    public int canonical_ids;


    public List<Result> results;


    @Override
    public String toString() {
        return "FCCResponse{" +
                "mutilcast_id=" + multicast_id +
                ", success=" + success +
                ", failiure=" + failure +
                ", canonical_ids=" + canonical_ids +
                ", results=" + results +
                '}';
    }
}
