package com.example.taskapp.archivoappadmin.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.taskapp.archivoappadmin.Models.GroupsTaskTime;
import com.example.taskapp.archivoappadmin.Models.GroupsTasks;
import com.example.taskapp.archivoappadmin.Models.MyTasks;
import com.example.taskapp.archivoappadmin.Models.SingleGroupTask;
import com.example.taskapp.archivoappadmin.Models.Statistics;
import com.example.taskapp.archivoappadmin.Models.StatisticsModel;
import com.example.taskapp.archivoappadmin.Models.User;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = "DatabaseHelper";


    //TODO This is Still under Development

    // Database Info
    private static final String DATABASE_NAME = "localdatabase";
    private static final int DATABASE_VERSION = 6;

    // Table Names
    private static final String TABLE_TASKS = "Tasks";
    private static final String TABLE_USERS = "Users";
    private static final String TABLE_GROUP_TASKS = "GroupTasks";
    private static final String TABLE_STATICS = "Statics";


    // Tasks Table Columns
    private static final String KEY_ID = "ID";
    private static final String KEY_TASK_ID = "TaskId";
    private static final String KEY_GROUP_TASK_ID = "GroupTaskID";
    private static final String KEY_TASK_Finish_Time = "finishTime";
    private static final String KEY_TASK_Start_Time = "startTime";
    private static final String KEY_TASK_Deadline = "deadline";
    private static final String KEY_TASK_Status = "status";
    private static final String KEY_TASK_CONTENT = "task_content";

    /**
     * Tasks table contains also
     * KEY_USER_UID
     * KEY_USER_ID
     */

    // User Table Columns
    private static final String KEY_USER_UID = "userUid";
    private static final String KEY_USER_ID = "userId";
    private static final String KEY_USER_NAME = "userName";
    private static final String KEY_USER_GROUP_ID = "groupId";


    // Statics Table Columns
    // KEY_USER_UID
    // KEY_USER_ID
    private static final String KEY_TOTAL_TASKS = "totalTasks";
    private static final String KEY_FINISH_ON_TIME = "finishOnTime";
    private static final String KEY_PENDING = "pending";
    private static final String KEY_FINISH_AFTER_TIME = "finishAfterTime";

    private static DatabaseHelper sInstance;

    public static synchronized DatabaseHelper getInstance(Context context) {
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.

        if (sInstance == null) {
            sInstance = new DatabaseHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Called when the database connection is being configured.
    // Configure database settings for things like foreign key support, write-ahead logging, etc.
    @Override
    public void onConfigure(SQLiteDatabase db) {
        super.onConfigure(db);
    }

    // Called when the database needs to be upgraded.
    // This method will only be called if a database already exists on disk with the same DATABASE_NAME,
    // but the DATABASE_VERSION is different than the version of the database that exists on disk.
    // Called when the database is created for the FIRST time.
    // If a database already exists on disk with the same DATABASE_NAME, this method will NOT be called.
    @Override
    public void onCreate(SQLiteDatabase db) {


        String CREATE_TASKS_TABLE = "CREATE TABLE " + TABLE_TASKS +
                "(" +
                KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + // Define a primary key
                KEY_USER_UID + " TEXT ," + // Define a foreign key
                KEY_TASK_ID + " TEXT," +
                KEY_TASK_Finish_Time + " TEXT," +
                KEY_TASK_Start_Time + " TEXT," +
                KEY_TASK_Deadline + " TEXT," +
                KEY_TASK_Status + " TEXT," +
                KEY_TASK_CONTENT + " TEXT," +
                KEY_USER_NAME + " TEXT," +
                KEY_USER_ID + " TEXT" +
                ")";

        String CREATE_GROUPS_TASKS_TABLE = "CREATE TABLE " + TABLE_GROUP_TASKS +
                "(" +
                KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                //   KEY_USER_UID + " TEXT REFERENCES " + TABLE_USERS + "," +
                KEY_USER_UID + " TEXT," +
                KEY_GROUP_TASK_ID + " TEXT," +
                KEY_TASK_CONTENT + " TEXT," +
                KEY_TASK_Deadline + " TEXT," +
                KEY_TASK_Start_Time + " TEXT," +
                KEY_TASK_Finish_Time + " TEXT," +
                KEY_TASK_Status + " TEXT" +
                ")";

        String CREATE_USERS_TABLE = "CREATE TABLE " + TABLE_USERS +
                "(" +
                KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                KEY_USER_NAME + " TEXT," +
                KEY_USER_ID + " TEXT," +
                KEY_USER_UID + " TEXT," +
                KEY_USER_GROUP_ID + " TEXT" +
                ")";


        String CREATE_STATICS_TABLE = "CREATE TABLE " + TABLE_STATICS +
                "(" +
                KEY_ID + " INTEGER PRIMARY KEY," +
                KEY_USER_UID + " TEXT, " +
                KEY_GROUP_TASK_ID + " TEXT," +
                KEY_TASK_CONTENT + " TEXT," +
                KEY_TOTAL_TASKS + " INTEGER," +
                KEY_FINISH_ON_TIME + " INTEGER," +
                KEY_PENDING + " INTEGER," +
                KEY_FINISH_AFTER_TIME + " INTEGER" +
                ")";


        db.execSQL(CREATE_TASKS_TABLE);
        db.execSQL(CREATE_USERS_TABLE);
        db.execSQL(CREATE_GROUPS_TASKS_TABLE);
        db.execSQL(CREATE_STATICS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion != newVersion) {
            // Simplest implementation is to drop all old tables and recreate them
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_GROUP_TASKS);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_TASKS);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_STATICS);

            onCreate(db);
        }
    }

    public void addOrUpdateTasks(MyTasks myTasks, String key) {
        // The database connection is cached so it's not expensive to call getWritableDatabase() multiple times.
        SQLiteDatabase db = getWritableDatabase();
        long groupTaskId = -1;
        Log.d(TAG, "addOrUpdateTasks: started ");
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_TASK_ID, key);
            values.put(KEY_USER_UID, myTasks.getUserUid());
            values.put(KEY_TASK_Finish_Time, myTasks.getFinishTime());
            values.put(KEY_TASK_Start_Time, myTasks.getStartTime());
            values.put(KEY_TASK_Deadline, myTasks.getDeadline());
            values.put(KEY_TASK_CONTENT, myTasks.getTask());
            values.put(KEY_TASK_Status, myTasks.getStatus());

            // First try to update the user in case the user already exists in the database
            // This assumes userNames are unique
            int rows = db.update(TABLE_TASKS, values, KEY_TASK_ID + "= ?", new String[]{key});

            // Check if update succeeded
            if (rows == 1) {
                // Get the primary key of the Tasks we just updated
                String usersSelectQuery = String.format("SELECT * FROM %s WHERE %s = ?",
                        TABLE_TASKS, KEY_TASK_ID);
                Cursor cursor = db.rawQuery(usersSelectQuery, new String[]{String.valueOf(key)});
                try {
                    if (cursor.moveToFirst()) {
                        groupTaskId = cursor.getInt(0);
                        db.setTransactionSuccessful();
                        Log.d(TAG, "addOrUpdateTask: Updated new task");
                    }
                } finally {
                    if (cursor != null && !cursor.isClosed()) {
                        cursor.close();
                        Log.d(TAG, "addOrUpdateTask: cursor close ");
                    }
                }
            } else {
                // user with this userName did not already exist, so insert new user
                Log.d(TAG, "addOrUpdateTask : added new task");
                groupTaskId = db.insertOrThrow(TABLE_TASKS, null, values);
                db.setTransactionSuccessful();
            }
        } catch (Exception e) {
            Log.d(TAG, "Error while trying to add or task Table " + e.getMessage());
        } finally {
            db.endTransaction();
        }

    }


    public void AddGroupTask(GroupsTasks groupsTasks, String key) {


        HashMap<String, GroupsTaskTime> hashMap = groupsTasks.getTimeTable();
        SingleGroupTask singleGroupTask = new SingleGroupTask();

        singleGroupTask.setGroupTaskID(key);
        singleGroupTask.setTaskContent(groupsTasks.getTask());
        singleGroupTask.setDeadline(groupsTasks.getDeadline());
        singleGroupTask.setGroupId(groupsTasks.getGroupId());
        singleGroupTask.setStatus("status");

        for (String uid : hashMap.keySet()) {

            GroupsTaskTime groupsTaskTime = hashMap.get(uid);
            singleGroupTask.setFinishTime(groupsTaskTime.getFinishTime());
            singleGroupTask.setStartTime(groupsTaskTime.getStartTime());
            singleGroupTask.setStatus(groupsTasks.getStatus());
            singleGroupTask.setUserUID(uid);

            addOrUpdateSingleGroupTask(singleGroupTask, key);
            Log.d(TAG, "AddGroupTask: " + singleGroupTask.toString());

        }


    }


    public void addOrUpdateSingleGroupTask(SingleGroupTask singleGroupTask, String key) {

        // The database connection is cached so it's not expensive to call getWritableDatabase() multiple times.
        SQLiteDatabase db = getWritableDatabase();
        long userId = -1;
        Log.d(TAG, "addOrUpdateUser: ");
        db.beginTransaction();

        try {
            ContentValues values = new ContentValues();
            values.put(KEY_GROUP_TASK_ID, key);
            values.put(KEY_USER_UID, singleGroupTask.getUserUID());
            values.put(KEY_TASK_CONTENT, singleGroupTask.getTaskContent());
            values.put(KEY_TASK_Deadline, singleGroupTask.getDeadline());
            values.put(KEY_TASK_Start_Time, singleGroupTask.getStartTime());
            values.put(KEY_TASK_Finish_Time, singleGroupTask.getFinishTime());
            values.put(KEY_TASK_Status, singleGroupTask.getStatus());

            // First try to update the user in case the user already exists in the database
            // This assumes userNames are unique
            int rows = db.update(TABLE_GROUP_TASKS, values, KEY_USER_UID + "= ? AND " + KEY_GROUP_TASK_ID + " = ?",
                    new String[]{singleGroupTask.getUserUID(), key});

            // Check if update succeeded
            if (rows == 1) {
                // Get the primary key of the user we just updated
                String usersSelectQuery = String.format("SELECT * FROM %s WHERE %s = ? AND %s = ? ",
                        TABLE_GROUP_TASKS, KEY_USER_UID, KEY_GROUP_TASK_ID);
                Cursor cursor = db.rawQuery(usersSelectQuery, new String[]{String.valueOf(singleGroupTask.getUserUID()), key});
                try {
                    if (cursor.moveToFirst()) {
                        userId = cursor.getInt(0);
                        db.setTransactionSuccessful();
                        Log.d(TAG, "addOrUpdateSingleGroupTask: updated single group task");
                    }
                } finally {
                    if (cursor != null && !cursor.isClosed()) {
                        cursor.close();
                        Log.d(TAG, "addOrUpdateSingleGroupTask:  cursor.close(); ");
                    }
                }
            } else {
                // user with this userName did not already exist, so insert new user
                Log.d(TAG, "addOrUpdateSingleGroupTask: added new GroupTask ");
                userId = db.insertOrThrow(TABLE_GROUP_TASKS, null, values);
                db.setTransactionSuccessful();
            }
        } catch (Exception e) {
            Log.d(TAG, "Error addOrUpdateSingleGroupTask " + e.getMessage());
        } finally {
            db.endTransaction();
        }

    }

    public long addOrUpdateUser(User user) {

        // The database connection is cached so it's not expensive to call getWritableDatabase() multiple times.
        SQLiteDatabase db = getWritableDatabase();
        long userId = -1;
        Log.d(TAG, "addOrUpdateUser: ");
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_USER_UID, user.getUserUid());
            values.put(KEY_USER_ID, user.getUserId());
            values.put(KEY_USER_NAME, user.getUserName());
            values.put(KEY_USER_GROUP_ID, user.getGroupId());

            // First try to update the user in case the user already exists in the database
            // This assumes userNames are unique
            int rows = db.update(TABLE_USERS, values, KEY_USER_UID + "= ?", new String[]{user.getUserUid()});

            // Check if update succeeded
            if (rows == 1) {
                // Get the primary key of the user we just updated
                String usersSelectQuery = String.format("SELECT * FROM %s WHERE %s = ?",
                        TABLE_USERS, KEY_USER_UID);
                Cursor cursor = db.rawQuery(usersSelectQuery, new String[]{String.valueOf(user.getUserUid())});
                try {
                    if (cursor.moveToFirst()) {
                        userId = cursor.getInt(0);
                        db.setTransactionSuccessful();
                        Log.d(TAG, "addOrUpdateUser: user Updated ");
                    }
                } finally {
                    if (cursor != null && !cursor.isClosed()) {
                        cursor.close();
                        Log.d(TAG, "addOrUpdateUser:  cursor.close(); ");
                    }
                }
            } else {
                // user with this userName did not already exist, so insert new user
                Log.d(TAG, "addOrUpdateUser: added new user");
                userId = db.insertOrThrow(TABLE_USERS, null, values);
                db.setTransactionSuccessful();
            }
        } catch (Exception e) {
            Log.d(TAG, "Error while trying to add or update user");
        } finally {
            db.endTransaction();
        }
        return userId;
    }


    public Statistics getUserTasksStatistics(User user) {
        Statistics statistics = new Statistics(user);
        String POSTS_SELECT_QUERY = String.format("SELECT * FROM %s WHERE %s =?", TABLE_TASKS, KEY_USER_UID);
        Log.d(TAG, "getUserTasksStatistics: " + POSTS_SELECT_QUERY);

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(POSTS_SELECT_QUERY, new String[]{user.getUserUid()});
        int pending = 0, count = 0, finishedAfterTime = 0, finishedOnTime = 0, started = 0;
        try {
            if (cursor.moveToFirst()) {
                do {
                    long finishTime = Long.parseLong(cursor.getString(cursor.getColumnIndex(KEY_TASK_Finish_Time)));
                    long startTime = Long.parseLong(cursor.getString(cursor.getColumnIndex(KEY_TASK_Start_Time)));
                    long deadline = Long.parseLong(cursor.getString(cursor.getColumnIndex(KEY_TASK_Deadline)));
                    count++;
                    if (startTime == 0) {
                        pending++;
                    } else if (finishTime == 0) {
                        started++;
                    } else {
                        if (CheckTimerStatus(deadline, finishTime)) {
                            finishedOnTime++;
                        } else {
                            finishedAfterTime++;
                        }
                    }
                } while (cursor.moveToNext());
                statistics.setFinishedAfterTime(finishedAfterTime);
                statistics.setFinishedOnTime(finishedOnTime);
                statistics.setStarted(started);
                statistics.setPending(pending);
                statistics.setTotal(count);
            }

        } catch (Exception e) {
            Log.d(TAG, "ERROR get UserTasksStatistics : " + e.getMessage());
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        cursor.close();
        db.close();
        return statistics;
    }

    public Statistics getUserGroupTasksStatistics(User user) {

        Statistics statistics = new Statistics(user);

        String POSTS_SELECT_QUERY = String.format("SELECT * FROM %s WHERE %s =?", TABLE_GROUP_TASKS, KEY_USER_UID);
        Log.d(TAG, "getUserTasksStatistics: " + POSTS_SELECT_QUERY);

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(POSTS_SELECT_QUERY, new String[]{user.getUserUid()});
        int pending = 0, count = 0, finishedAfterTime = 0, finishedOnTime = 0, started = 0;
        try {
            if (cursor.moveToFirst()) {
                do {
                    long finishTime = Long.parseLong(cursor.getString(cursor.getColumnIndex(KEY_TASK_Finish_Time)));
                    long startTime = Long.parseLong(cursor.getString(cursor.getColumnIndex(KEY_TASK_Start_Time)));
                    long deadline = Long.parseLong(cursor.getString(cursor.getColumnIndex(KEY_TASK_Deadline)));
                    count++;
                    if (startTime == 0) {
                        pending++;
                    } else if (finishTime == 0) {
                        started++;
                    } else {
                        if (CheckTimerStatus(deadline, finishTime)) {
                            finishedOnTime++;
                        } else {
                            finishedAfterTime++;
                        }
                    }
                } while (cursor.moveToNext());
                statistics.setFinishedAfterTime(finishedAfterTime);
                statistics.setFinishedOnTime(finishedOnTime);
                statistics.setStarted(started);
                statistics.setPending(pending);
                statistics.setTotal(count);
            }

        } catch (Exception e) {
            Log.d(TAG, "ERROR get UserTasksStatistics : " + e.getMessage());
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        cursor.close();
        db.close();
        return statistics;
    }

    public static Boolean CheckTimerStatus(long DurationTime, long finishTime) {
        Log.d("STATS", "CheckTimerStatus: ");
        Boolean check;

        Calendar calendar = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        calendar.setTimeInMillis(DurationTime);
        calendar2.setTimeInMillis(finishTime);


        if (calendar2.before(calendar)) {
            check = true;
        } else {
            check = true;
        }

        return check;
    }

    // Get all Users in the database
    public List<User> getAllUsers() {
        List<User> users = new ArrayList<>();

        // SELECT * FROM POSTS
        // LEFT OUTER JOIN USERS
        // ON POSTS.KEY_POST_USER_ID_FK = USERS.KEY_USER_ID
        String POSTS_SELECT_QUERY = String.format("SELECT * FROM %s", TABLE_USERS);

        // "getReadableDatabase()" and "getWriteableDatabase()" return the same object (except under low
        // disk space scenarios)
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(POSTS_SELECT_QUERY, null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    User newUser = new User();
                    newUser.setUserName(cursor.getString(cursor.getColumnIndex(KEY_USER_NAME)));
                    newUser.setUserUid(cursor.getString(cursor.getColumnIndex(KEY_USER_UID)));

                    users.add(newUser);

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.d(TAG, "Error while trying to get posts from database");
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return users;
    }

    public User getUsers(String uid) {
        User users = new User();

        // SELECT * FROM POSTS
        // LEFT OUTER JOIN USERS
        // ON POSTS.KEY_POST_USER_ID_FK = USERS.KEY_USER_ID
        String POSTS_SELECT_QUERY = String.format("SELECT * FROM %s WHERE %s =?", TABLE_USERS, KEY_USER_UID);
        // "getReadableDatabase()" and "getWriteableDatabase()" return the same object (except under low
        // disk space scenarios)
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(POSTS_SELECT_QUERY, new String[]{uid});
        try {
            if (cursor.moveToFirst()) {
                do {
                    users.setUserName(cursor.getString(cursor.getColumnIndex(KEY_USER_NAME)));
                    users.setUserUid(cursor.getString(cursor.getColumnIndex(KEY_USER_UID)));
                    users.setUserId(cursor.getString(cursor.getColumnIndex(KEY_USER_ID)));
                    users.setGroupId(cursor.getString(cursor.getColumnIndex(KEY_USER_GROUP_ID)));

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.d(TAG, "Error while trying to get posts from database");
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return users;
    }

    public void clearAllRecords() {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            // Order of deletions is important when foreign key relationships exist.
            db.delete(TABLE_TASKS, null, null);
            db.delete(TABLE_USERS, null, null);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.d(TAG, "Error while trying to delete all posts and users");
        } finally {
            db.endTransaction();
        }
    }

    public void UpdateStatistics() {
        List<User> list = getAllUsers();
        for (User user : list) {
            Statistics asIndividual = getUserTasksStatistics(user);
            Statistics asGroup = getUserGroupTasksStatistics(user);
            addOrUpdateStatistics(asIndividual.sum(asGroup));
        }
    }

    public void addOrUpdateStatistics(Statistics statistics) {

        // The database connection is cached so it's not expensive to call getWritableDatabase() multiple times.
        SQLiteDatabase db = getWritableDatabase();
        long userId = -1;
        Log.d(TAG, "addOrUpdateStatistics: ");
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_USER_UID, statistics.getUserUid());
            values.put(KEY_TOTAL_TASKS, statistics.getTotal());
            values.put(KEY_FINISH_ON_TIME, statistics.getFinishedOnTime());
            values.put(KEY_FINISH_AFTER_TIME, statistics.getFinishedAfterTime());
            values.put(KEY_PENDING, statistics.getPending());

            // First try to update the user in case the user already exists in the database
            // This assumes userNames are unique
            int rows = db.update(TABLE_STATICS, values, KEY_USER_UID + "= ?", new String[]{statistics.getUserUid()});

            // Check if update succeeded
            if (rows == 1) {
                // Get the primary key of the user we just updated
                String usersSelectQuery = String.format("SELECT * FROM %s WHERE %s = ?",
                        TABLE_STATICS, KEY_USER_UID);
                Cursor cursor = db.rawQuery(usersSelectQuery, new String[]{String.valueOf(statistics.getUserUid())});
                try {
                    if (cursor.moveToFirst()) {
                        db.setTransactionSuccessful();
                        Log.d(TAG, "addOrUpdateStatistics: Statistics Updated ");
                    }
                } finally {
                    if (cursor != null && !cursor.isClosed()) {
                        cursor.close();
                        Log.d(TAG, "addOrUpdateStatistics:  cursor.close(); ");
                    }
                }
            } else {
                // user with this userName did not already exist, so insert new user
                Log.d(TAG, "addOrUpdateUser: added new user");
                userId = db.insertOrThrow(TABLE_STATICS, null, values);
                db.setTransactionSuccessful();
            }
        } catch (Exception e) {
            Log.d(TAG, "Error while trying to add or update TABLE_STATICS" + e.getMessage());
        } finally {
            db.endTransaction();
        }

    }


    public ArrayList<Statistics> getAllStatistics() {
        ArrayList<Statistics> Stats = new ArrayList<>();
        // SELECT * FROM POSTS
        // LEFT OUTER JOIN USERS
        // ON POSTS.KEY_POST_USER_ID_FK = USERS.KEY_USER_ID
        String POSTS_SELECT_QUERY = String.format("SELECT * FROM %s", TABLE_STATICS);

        // "getReadableDatabase()" and "getWriteableDatabase()" return the same object (except under low
        // disk space scenarios)

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(POSTS_SELECT_QUERY, null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    Statistics statistics = new Statistics();

                    statistics.setUserUid(cursor.getString(cursor.getColumnIndex(KEY_USER_UID)));
                    statistics.setFinishedOnTime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(KEY_FINISH_ON_TIME))));
                    statistics.setFinishedAfterTime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(KEY_FINISH_AFTER_TIME))));
                    statistics.setPending(Integer.parseInt(cursor.getString(cursor.getColumnIndex(KEY_PENDING))));
                    statistics.setTotal(Integer.parseInt(cursor.getString(cursor.getColumnIndex(KEY_TOTAL_TASKS))));

                    User user = getUsers(statistics.getUserUid());

                    statistics.setName(user.getUserName());
                    statistics.setUserId(user.getUserId());

                    Stats.add(statistics);

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.d(TAG, "Error while trying to get posts from database");
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return Stats;
    }

    public Statistics getUserStatistics(String uid) {

        Statistics statistics = new Statistics(getUsers(uid));
        User user = getUsers(uid);
        String POSTS_SELECT_QUERY = String.format("SELECT * FROM %s WHERE %s =?", TABLE_STATICS, KEY_USER_UID);

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(POSTS_SELECT_QUERY, new String[]{uid});

        try {
            if (cursor.moveToFirst()) {
                do {
                    statistics.setUserUid(cursor.getString(cursor.getColumnIndex(KEY_USER_UID)));
                    statistics.setFinishedOnTime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(KEY_FINISH_ON_TIME))));
                    statistics.setFinishedAfterTime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(KEY_FINISH_AFTER_TIME))));
                    statistics.setPending(Integer.parseInt(cursor.getString(cursor.getColumnIndex(KEY_PENDING))));
                    statistics.setTotal(Integer.parseInt(cursor.getString(cursor.getColumnIndex(KEY_TOTAL_TASKS))));
                    statistics.setName(user.getUserName());
                    statistics.setUserId(user.getUserId());
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.d(TAG, "Error while trying to get posts from database");
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }


        return statistics;
    }
}
