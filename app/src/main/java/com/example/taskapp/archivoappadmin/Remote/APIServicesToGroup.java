package com.example.taskapp.archivoappadmin.Remote;

import com.example.taskapp.archivoappadmin.Models.NotificationModel.FCCResponse;
import com.example.taskapp.archivoappadmin.Models.NotificationModel.Sender;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIServicesToGroup {
    @Headers(
            {
                    "Content-Type:application/json",
                    "Authorization:key=AAAA2QBIpYI:APA91bHxNt_kHX84fNVibz938MO5b466kAdeoUO1hIXJ-kzC-tdhwzK6T1k7xyFVg6fyV22iFbgHX6hUveL_Xxg647VwZShe_Gyn8tb-A9nSDiqEmYSWLiwpCbbqyN4O2LvOetJ_Lemj"
            }
    )


    @POST("fcm/send")
    Call<FCCResponse> sendNotificationToGroup(@Body Sender body);

}
