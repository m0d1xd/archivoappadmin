package com.example.taskapp.archivoappadmin.Models;


import java.util.HashMap;

public class GroupsTasks {

    private String task, groupId, status;
    private long deadline;
    private HashMap<String, GroupsTaskTime> timeTable;

    public GroupsTasks() {
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getDeadline() {
        return deadline;
    }

    public void setDeadline(long deadline) {
        this.deadline = deadline;
    }

    public HashMap<String, GroupsTaskTime> getTimeTable() {
        return timeTable;
    }

    public void setTimeTable(HashMap<String, GroupsTaskTime> timeTable) {
        this.timeTable = timeTable;
    }
}

