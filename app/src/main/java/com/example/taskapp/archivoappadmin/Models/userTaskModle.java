package com.example.taskapp.archivoappadmin.Models;

public class userTaskModle {

    private String uid;
    private long startTime, finishTime;

    public userTaskModle(String uid, long startTime, long finishTime) {
        this.uid = uid;
        this.startTime = startTime;
        this.finishTime = finishTime;
    }

    public userTaskModle() {
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(long finishTime) {
        this.finishTime = finishTime;
    }
}
