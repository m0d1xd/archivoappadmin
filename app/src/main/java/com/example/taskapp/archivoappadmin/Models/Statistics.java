package com.example.taskapp.archivoappadmin.Models;

import java.util.Calendar;

public class Statistics {


    private String userId, userName, userUid;
    private int total, started, finishedOnTime, finishedAfterTime, pending;
    private float avg;

    public Statistics() {

    }

    public int getStarted() {
        return started;
    }

    public void setStarted(int started) {
        this.started = started;
    }

    public Statistics(User user) {
        this.userId = user.getUserId();
        this.userName = user.getUserName();
        this.userUid = user.getUserUid();
        this.total = 0;
        this.started = 0;
        this.finishedOnTime = 0;
        this.finishedAfterTime = 0;
        this.pending = 0;
        this.avg = 0;
    }

    public String getUserUid() {
        return userUid;
    }

    public void setUserUid(String userUid) {
        this.userUid = userUid;
    }

    public float getAvg() {
        return ((float) finishedOnTime / (float) total) * 100;
    }

    public int getPending() {
        return pending;
    }

    public void setPending(int pending) {
        this.pending = pending;
    }

    public void setAvg() {
        this.avg = ((float) finishedOnTime / (float) total) * 100;
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return userName;
    }

    public void setName(String name) {
        this.userName = name;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getFinishedOnTime() {
        return finishedOnTime;
    }

    public void setFinishedOnTime(int finishedOnTime) {
        this.finishedOnTime = finishedOnTime;
    }

    public int getFinishedAfterTime() {
        return finishedAfterTime;
    }

    public void setFinishedAfterTime(int finishedAfterTime) {
        this.finishedAfterTime = finishedAfterTime;
    }

    @Override
    public String toString() {
        return "Statistics{" +
                "userId='" + userId + '\'' +
                ", name='" + userName + '\'' +
                ", userUid='" + userUid + '\'' +
                ", total=" + total +
                ", started=" + started +
                ", finishedOnTime=" + finishedOnTime +
                ", finishedAfterTime=" + finishedAfterTime +
                ", pending=" + pending +
                ", avg=" + avg +
                '}';
    }

    public void setAvg(float avg) {
        this.avg = avg;
    }

    private static Boolean CheckTimerStatus(long DurationTime, long finishTime) {

        Calendar calendar = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        calendar.setTimeInMillis(DurationTime);
        calendar2.setTimeInMillis(finishTime);

        return calendar2.before(calendar);
    }

    public Statistics sum(Statistics s) {
        Statistics newStats = new Statistics();
        newStats.setUserId(s.getUserId());
        newStats.setUserUid(s.getUserUid());
        newStats.setName(s.getName());
        newStats.setTotal(this.total = total + s.getTotal());
        newStats.setFinishedOnTime(this.finishedOnTime = finishedOnTime + s.getFinishedOnTime());
        newStats.setFinishedAfterTime(this.finishedAfterTime = finishedAfterTime + s.getFinishedAfterTime());
        newStats.setPending(this.pending = s.getPending() + pending);
        newStats.setAvg();

        return newStats;
    }
}
