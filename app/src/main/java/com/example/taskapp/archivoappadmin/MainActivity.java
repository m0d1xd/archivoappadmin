package com.example.taskapp.archivoappadmin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.taskapp.archivoappadmin.Activites.Authentication.LoginActivity;
import com.example.taskapp.archivoappadmin.Activites.HomeActivity;
import com.example.taskapp.archivoappadmin.Common.Common;
import com.example.taskapp.archivoappadmin.Models.Staff;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import static com.example.taskapp.archivoappadmin.Common.Common.KEY_INTENT_EXTRA_MODEL;

import static com.example.taskapp.archivoappadmin.Common.Common.KEY_QUERY_FILTER_USER_ID;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_TABLE_STAFF;
import static com.example.taskapp.archivoappadmin.Common.Common.UpdateToken;

public class MainActivity extends AppCompatActivity {


    private static final String TAG = "MainActivity";

    private EditText edt_id;

    private Button btn_submit;


    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseUser firebaseUser;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edt_id = findViewById(R.id.edt_id_idactivity);
        btn_submit = findViewById(R.id.btn_submit_idactivity);
        //Firebase Auth . to keep user Signed in

        mAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();

        databaseReference = firebaseDatabase.getReference(KEY_TABLE_STAFF);

        final ProgressDialog mDialog = new ProgressDialog(MainActivity.this);
        mDialog.setMessage("Please Wait ..");


        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                mDialog.show();
                firebaseUser = firebaseAuth.getCurrentUser();
                if (firebaseUser != null) {
                    DatabaseReference user_info = databaseReference;
                    user_info.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            try {
                                Common.CurrentStaff = dataSnapshot.child(firebaseUser.getUid()).getValue(Staff.class);
                                Log.d(TAG, "onDataChange: " + Common.CurrentStaff.toString());
                                UpdateToken(FirebaseInstanceId.getInstance().getToken());
                                mDialog.dismiss();
                                AutoLogin();
                            } catch (Exception e) {
                                mDialog.dismiss();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            mDialog.dismiss();
                        }
                    });

                } else if (firebaseUser == null) {
                    Log.d(TAG, "onAuthStateChanged: user siend out");
                    mDialog.dismiss();
                } else {
                    mDialog.dismiss();

                }
            }
        };
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkValidation(edt_id);
                mDialog.show();
                DatabaseReference ref = firebaseDatabase.getReference(KEY_TABLE_STAFF);
                Query query = ref.orderByChild(KEY_QUERY_FILTER_USER_ID).equalTo(edt_id.getText().toString());
                query.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot ds : dataSnapshot.getChildren()) {
                            Staff staff = ds.getValue(Staff.class);
                            if (staff != null) {
                                Common.CurrentStaff = staff;
                                mDialog.dismiss();
                                Log.d(TAG, "onDataChange: " + staff.getEmail());
                                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                                Log.d(TAG, "onDataChange: " + staff.toString());
                                intent.putExtra(KEY_INTENT_EXTRA_MODEL, staff);
                                startActivity(intent);
                            } else {
                                mDialog.dismiss();
                                Toast.makeText(MainActivity.this, "No users with this id", Toast.LENGTH_SHORT).show();
                            }

                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }
        });
    }

    private void AutoLogin() {
        Intent i = new Intent(MainActivity.this, HomeActivity.class);
        startActivity(i);
        finish();
    }

    public void checkValidation(EditText editTextID) {
        String id;
        id = editTextID.getText().toString();
        if (id.isEmpty()) {
            editTextID.setError(getString(R.string.input_error_ID));
            editTextID.requestFocus();
            return;
        }
        if (!Common.checkIdFormat(id)) {
            editTextID.setError(getString(R.string.input_error_Format_ID));
            editTextID.requestFocus();
            return;
        }

    }


    @Override
    protected void onStart() {
        super.onStart();
        if (Common.IsConnectedToInternet(getApplicationContext()))
            mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (Common.IsConnectedToInternet(getApplicationContext()))
            mAuth.removeAuthStateListener(mAuthListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (Common.IsConnectedToInternet(getApplicationContext()))
            mAuth.removeAuthStateListener(mAuthListener);
    }
}
