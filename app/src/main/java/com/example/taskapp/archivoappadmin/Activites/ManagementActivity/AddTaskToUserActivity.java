package com.example.taskapp.archivoappadmin.Activites.ManagementActivity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.taskapp.archivoappadmin.Common.Common;
import com.example.taskapp.archivoappadmin.Models.MyTasks;
import com.example.taskapp.archivoappadmin.Models.NotificationModel.FCCResponse;
import com.example.taskapp.archivoappadmin.Models.NotificationModel.Notification;
import com.example.taskapp.archivoappadmin.Models.NotificationModel.Sender;
import com.example.taskapp.archivoappadmin.Models.NotificationModel.Token;
import com.example.taskapp.archivoappadmin.Models.User;
import com.example.taskapp.archivoappadmin.R;
import com.example.taskapp.archivoappadmin.Remote.APIServices;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.taskapp.archivoappadmin.Common.Common.KEY_INTENT_EXTRA_USER;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_STATUS_PENDING;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_TABLE_TASK;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_TABLE_TOKENS;

public class AddTaskToUserActivity extends AppCompatActivity {
    private static final String TAG = "AdminWrightTaskActivity";

    //Defining Firebase database
    private FirebaseDatabase database;
    private DatabaseReference FirebaseTasks;

    private TextView tv_PersonAssignedTo, tv_back, tv_assignnow;
    private EditText edt_writetask_writetaskactvy, edt_time_admintaskactvy;


    ProgressDialog progressDialog;

    int mHour, mMinute, mYear, mMonth, mDay;
    long millis;
    String taskDate, date_time;
    private APIServices mServices;

    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task_to_user);

        if (getIntent() != null) {
            user = getIntent().getParcelableExtra(KEY_INTENT_EXTRA_USER);
        }

        mServices = Common.getFCMService();

        //Init FireBase

        database = FirebaseDatabase.getInstance();
        FirebaseTasks = database.getReference(KEY_TABLE_TASK);


        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Assigning Task");


        tv_PersonAssignedTo = (TextView) findViewById(R.id.tv_PersonAssignedTo);


        // user = getIntent().getParcelableExtra(KEY_STAFF_USER_ID);
        tv_PersonAssignedTo.setText("You've Chosen " + user.getUserName() + "\n to do the Task");
        edt_writetask_writetaskactvy = (EditText) findViewById(R.id.edt_writeTask);


        edt_time_admintaskactvy = (EditText) findViewById(R.id.edt_time);
        edt_time_admintaskactvy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePicker();

            }
        });


        tv_assignnow = (TextView) findViewById(R.id.tv_assignNow);
        tv_assignnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddTaskToUser();
            }
        });

        tv_back = (TextView) findViewById(R.id.tv_back);
        tv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();

            }
        });

    }

    private void datePicker() {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {


                        date_time = year + "/" + (monthOfYear + 1) + "/" + dayOfMonth;
                        //*************Call Time Picker Here ********************
                        timePicker();
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    private void timePicker() {
        // Get Current Time
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        final TimePickerDialog timePickerDialog = new TimePickerDialog(AddTaskToUserActivity.this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                        mHour = hourOfDay;
                        mMinute = minute;
                        taskDate = date_time + " " + mHour + ":" + mMinute;


                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
                        Date date = null;
                        try {
                            date = sdf.parse(taskDate);
                            millis = date.getTime();
                            long time = System.currentTimeMillis();
                            if (millis < time) {
                                Toast.makeText(AddTaskToUserActivity.this, " Select time in future..", Toast.LENGTH_SHORT).show();

                                return;
                            }
                            edt_time_admintaskactvy.setText(taskDate);
                            Log.i("Log", " millis: " + millis);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                    }
                }, mHour, mMinute, true);

        timePickerDialog.show();

    }

    private void sendTaskStatusNotificationToUser(final String Key, final String userUid) {
        DatabaseReference tokens = database.getReference(KEY_TABLE_TOKENS);
        tokens.orderByKey().equalTo(userUid)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot postDatasnapShot : dataSnapshot.getChildren()) {
                            Token token = postDatasnapShot.getValue(Token.class);
                            Log.d(TAG, "onDataChange: 197 " + token.getToken());
                            //Making raw Payload
                            Notification notification = new Notification("You have new Task from " + Key, "Task App");
                            Sender content = new Sender(token.getToken(), notification);
                            Log.d(TAG, "onDataChange: 201 " + content.toString());
                            mServices.sendNotification(content).enqueue(new Callback<FCCResponse>() {
                                @Override
                                public void onResponse(Call<FCCResponse> call, Response<FCCResponse> response) {
                                    if (response.body().success == 1) {

                                        Toast.makeText(AddTaskToUserActivity.this, "Task Assigned Successfuly", Toast.LENGTH_SHORT).show();
                                        Log.d(TAG, "onResponse: " + "Task Assigned Successfuly");


                                    } else {
                                        Toast.makeText(AddTaskToUserActivity.this, " Task Assigned Successfuly,but faild to send notification", Toast.LENGTH_SHORT).show();
                                        Log.d(TAG, "onResponse: " + "Task Assigned Successfuly");
                                    }
                                }

                                @Override
                                public void onFailure(Call<FCCResponse> call, Throwable t) {
                                    Log.d(TAG, "onFailure: " + t.getMessage());
                                }
                            });
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.d(TAG, "onCancelled: " + databaseError.getDetails());
                    }

                });
    }

    public void AddTaskToUser() {

        String TaskString = edt_writetask_writetaskactvy.getText().toString();

        if (!TaskString.equals("")) {

            progressDialog.show();

            //This is A Primary key for each Task
            String Current_Task = String.valueOf(System.currentTimeMillis());
            MyTasks myTasks = new MyTasks();
            myTasks.setUserUid(user.getUserUid());
            myTasks.setDeadline(millis);
            myTasks.setStatus(KEY_STATUS_PENDING);
            myTasks.setTask(TaskString);
            myTasks.setStartTime(0);
            myTasks.setUserId(user.getUserId());
            myTasks.setUserName(user.getUserName());
            progressDialog.dismiss();
            Toast.makeText(getApplicationContext(), "Done", Toast.LENGTH_SHORT).show();

            Log.d(TAG, "AddTaskToUser: Curent millis :" + Common.getDate(Long.parseLong(Current_Task))
                    + "| Timer:" + Common.getDate(millis));
            FirebaseTasks.child(Current_Task).setValue(myTasks);

            sendTaskStatusNotificationToUser(Common.CurrentStaff.getUserName(), user.getUserUid());

            finish();


        }
    }

}
