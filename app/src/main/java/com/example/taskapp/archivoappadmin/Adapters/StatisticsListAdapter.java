package com.example.taskapp.archivoappadmin.Adapters;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.taskapp.archivoappadmin.Database.DatabaseHelper;
import com.example.taskapp.archivoappadmin.Interface.ItemClickListener;
import com.example.taskapp.archivoappadmin.Models.Statistics;
import com.example.taskapp.archivoappadmin.Models.User;
import com.example.taskapp.archivoappadmin.R;

import java.util.ArrayList;
import java.util.Objects;

public class StatisticsListAdapter extends
        RecyclerView.Adapter<StatisticsListAdapter.StatisticsViewHolder> {

    private ArrayList<Statistics> ListData;
    private Context mContext;
    private DatabaseHelper databaseHelper;

    public StatisticsListAdapter(Context context, ArrayList<Statistics> statistics, String Condition, DatabaseHelper databaseHelper) {
        mContext = context;
        this.ListData = FilterArray(statistics, Condition);
        this.databaseHelper = databaseHelper;
    }

    @NonNull
    @Override
    public StatisticsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.rv_statistics_list_row, parent, false);

        return new StatisticsViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull StatisticsViewHolder holder, int position) {

        float Average = ListData.get(position).getAvg();
        final User user = databaseHelper.getUsers(ListData.get(position).getUserUid());

        String userName = user.getUserName();
        final String userID = user.getUserId();
        final String userUid = user.getUserUid();


        holder.statics_tv_userName.setText("User Name :" + userName);
        holder.statics_tv_average.setText("Average : " + String.valueOf(Average));
        holder.statics_tv_userId.setText("User ID : " + userID);
        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                Statistics userStats = databaseHelper.getUserStatistics(userUid);
                showDialog(userStats, user);
                //  showStatisticsDialog(userStats, user);
            }
        });
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @SuppressLint("SetTextI18n")
    private void showDialog(Statistics userStats, User user) {
        final Dialog dia = new Dialog(mContext);
        dia.setContentView(R.layout.dialog_statistics);
        dia.setTitle("User Statistics");
        Objects.requireNonNull(dia.getWindow()).clearFlags(WindowManager.LayoutParams.FIRST_SUB_WINDOW);
        dia.setCancelable(true);

        //  TextView user_Name = dia.findViewById(R.id.avg_tv_userName);
        TextView avg_tv_userName,
                avg_tv_userId,
                avg_tv_personGroupID,
                avg_onTime,
                avg_afterTime,
                avg_pendingTasks,
                tv_avarage,
                avg_TotalTasks;

        avg_tv_userName = dia.findViewById(R.id.avg_tv_userName);
        avg_tv_userId = dia.findViewById(R.id.avg_tv_userId);
        avg_tv_personGroupID = dia.findViewById(R.id.avg_tv_personGroupID);
        avg_onTime = dia.findViewById(R.id.avg_onTime);
        avg_afterTime = dia.findViewById(R.id.avg_afterTime);
        avg_pendingTasks = dia.findViewById(R.id.avg_pendingTasks);
        tv_avarage = dia.findViewById(R.id.tv_avarage);
        avg_TotalTasks = dia.findViewById(R.id.avg_TotalTasks);

        avg_afterTime.setText("Finished after time : " + userStats.getFinishedAfterTime());
        avg_onTime.setText("Finished on time : " + String.valueOf(userStats.getFinishedOnTime()));
        avg_TotalTasks.setText("Total : " + String.valueOf(userStats.getTotal()));
        avg_pendingTasks.setText("Pending : " + String.valueOf(userStats.getPending()));
        avg_tv_userName.setText("User Name :" + userStats.getName());
        avg_tv_userId.setText("User Id : " + user.getGroupId());
        avg_tv_personGroupID.setText("Group ID : " + user.getGroupId());
        userStats.setAvg();

        if (userStats.getTotal() == 0) {
            tv_avarage.setText("User Still not rated  0 Tasks ");
        } else
            tv_avarage.setText("Average :" + userStats.getAvg());
        dia.show();


    }

    @SuppressLint("SetTextI18n")
    private void showStatisticsDialog(Statistics userStats, User user) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

        alertDialog.setTitle("Statistic for User " + user.getUserName());

//        Lay   outInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LayoutInflater inflater = LayoutInflater.from(mContext);

        View statistics_view = inflater.inflate(R.layout.dialog_statistics, null);
        alertDialog.setView(statistics_view);

        TextView avg_tv_userName,
                avg_tv_userId,
                avg_tv_personGroupID,
                avg_onTime,
                avg_afterTime,
                avg_pendingTasks,
                tv_avarage,
                avg_TotalTasks;

        avg_tv_userName = statistics_view.findViewById(R.id.avg_tv_userName);
        avg_tv_userId = statistics_view.findViewById(R.id.avg_tv_userId);
        avg_tv_personGroupID = statistics_view.findViewById(R.id.avg_tv_personGroupID);
        avg_onTime = statistics_view.findViewById(R.id.avg_onTime);
        avg_afterTime = statistics_view.findViewById(R.id.avg_afterTime);
        avg_pendingTasks = statistics_view.findViewById(R.id.avg_pendingTasks);
        tv_avarage = statistics_view.findViewById(R.id.tv_avarage);
        avg_TotalTasks = statistics_view.findViewById(R.id.avg_TotalTasks);


        avg_afterTime.setText("Finished after time : " + userStats.getFinishedAfterTime());
        avg_onTime.setText("Finished on time : " + String.valueOf(userStats.getFinishedOnTime()));
        avg_TotalTasks.setText("Total : " + String.valueOf(userStats.getTotal()));
        avg_pendingTasks.setText("Pending : " + String.valueOf(userStats.getPending()));
        avg_tv_userName.setText("User Name :" + userStats.getName());
        avg_tv_userId.setText("User Id : " + user.getGroupId());
        avg_tv_personGroupID.setText("Group ID : " + user.getGroupId());
        userStats.setAvg();

        if (userStats.getTotal() == 0) {
            tv_avarage.setText("User Still not rated  0 Tasks ");
        } else
            tv_avarage.setText("Average :" + userStats.getAvg());


        alertDialog.show();

        alertDialog.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();

            }
        });


    }

    public ArrayList<Statistics> FilterArray(ArrayList<Statistics> list, String Condition) {
        ArrayList<Statistics> filter = null;
        this.ListData = list;
        if (Condition.equals("ALL")) {
            filter = this.ListData;
        } else if (Condition.equals("Users More than 50% completed tasks")) {
            filter = new ArrayList<>();
            for (Statistics model : ListData) {
                model.setAvg();
                if (model.getAvg() > 50) {
                    filter.add(model);
                }
            }
        } else if (Condition.equals("Users Less than 50% completed tasks")) {
            filter = new ArrayList<>();
            for (Statistics model : ListData) {
                model.setAvg();
                if (model.getAvg() < 50) {
                    filter.add(model);
                }
            }

        } else if (Condition.equals("Users with 0 finished Tasks")) {
            filter = new ArrayList<>();
            for (Statistics model : ListData) {
                model.setAvg();
                if (model.getAvg() == 0) {
                    filter.add(model);
                }
            }
        }
        return filter;
    }

    class StatisticsViewHolder extends RecyclerView.ViewHolder implements
            View.OnClickListener {

        public TextView
                statics_tv_userName,
                statics_tv_userId,
                statics_tv_average;


        private ItemClickListener itemClickListener;

        public StatisticsViewHolder(View itemView) {
            super(itemView);

            statics_tv_userName = itemView.findViewById(R.id.statics_tv_userName);
            statics_tv_userId = itemView.findViewById(R.id.statics_tv_userId);
            statics_tv_average = itemView.findViewById(R.id.statics_rating_avg);


            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            itemClickListener.onClick(v, getAdapterPosition(), false);

        }

        public void setItemClickListener(ItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
        }

    }

    @Override
    public int getItemCount() {
        if (ListData == null)
            return 0;
        else
            return ListData.size();
    }

}
