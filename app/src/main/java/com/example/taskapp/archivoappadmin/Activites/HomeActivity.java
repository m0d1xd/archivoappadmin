package com.example.taskapp.archivoappadmin.Activites;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.taskapp.archivoappadmin.Activites.CheckingActivities.StatisticsActivity;
import com.example.taskapp.archivoappadmin.Activites.CheckingActivities.TaskHistoryCheck;
import com.example.taskapp.archivoappadmin.Activites.ManagementActivity.PersonListActivity;
import com.example.taskapp.archivoappadmin.Activites.Results.ResultActivity;
import com.example.taskapp.archivoappadmin.Activites.Superior.SuperiorActivity;
import com.example.taskapp.archivoappadmin.Common.Common;
import com.example.taskapp.archivoappadmin.MainActivity;
import com.example.taskapp.archivoappadmin.Models.Groups;
import com.example.taskapp.archivoappadmin.R;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static com.example.taskapp.archivoappadmin.Common.Common.KEY_TABLE_GROUPS;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    private final String TAG = "HomeActivity";
    private ValueEventListener GroupsvalueEventListener;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    // private Query groupsQuery;

    private TextView tv_welcome;

    private ImageView iv_assigntast,
            iv_results,
            iv_checkStatus,
            iv_EditPerson,
            iv_statistics,
            iv_logout;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        //  new Database(this).fillData();

        try {
            tv_welcome.setText("Welcome,\n" + Common.CurrentStaff.getUserName());

        } catch (Exception e) {

        }


        firebaseDatabase = FirebaseDatabase.getInstance();

        databaseReference = firebaseDatabase.getReference(KEY_TABLE_GROUPS);

        iv_EditPerson = findViewById(R.id.iv_EditPerson);

        tv_welcome = findViewById(R.id.tv_welcome);

        iv_logout = findViewById(R.id.iv_logout);

        iv_statistics = findViewById(R.id.iv_statistics);

        iv_assigntast = findViewById(R.id.iv_assignTast);

        iv_checkStatus = findViewById(R.id.iv_checkStatus);

        iv_results = findViewById(R.id.iv_results);

        iv_assigntast.setOnClickListener(this);

        iv_checkStatus.setOnClickListener(this);

        iv_results.setOnClickListener(this);

        iv_logout.setOnClickListener(this);

        iv_statistics.setOnClickListener(this);

        iv_EditPerson.setOnClickListener(this);

        GroupsvalueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Common.GroupList = new ArrayList<>();
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    Groups group = ds.getValue(Groups.class);
                    Common.GroupList.add(group);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

    }

    @Override
    public void onClick(View view) {
        Intent i = null;
        switch (view.getId()) {
            case R.id.iv_assignTast:
                i = new Intent(HomeActivity.this, PersonListActivity.class);
                break;
            case R.id.iv_checkStatus:
                i = new Intent(HomeActivity.this, TaskHistoryCheck.class);
                break;
            case R.id.iv_results:
                i = new Intent(HomeActivity.this, ResultActivity.class);
                break;
            case R.id.iv_statistics:
                i = new Intent(HomeActivity.this, StatisticsActivity.class);
                break;
            case R.id.iv_EditPerson:
                if (Common.CurrentStaff.getRank() == 0) {
                    i = new Intent(this, SuperiorActivity.class);
                } else {
                    Toast.makeText(this, "Only Superior admin allowed", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.iv_logout:
                SignOut();
                break;
        }
        if (i != null)
            startActivity(i);
    }

    public void SignOut() {
        FirebaseAuth.getInstance().signOut();
        startActivity(new Intent(HomeActivity.this, MainActivity.class));
        Toast.makeText(getApplicationContext(), "Good Bye", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (Common.GroupList == null)
            databaseReference.addValueEventListener(GroupsvalueEventListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (Common.GroupList != null)
            databaseReference.addValueEventListener(GroupsvalueEventListener);
    }
}
