package com.example.taskapp.archivoappadmin.Models.NotificationModel;

import com.google.gson.annotations.SerializedName;

public class Topics {

    @SerializedName("topic")
    private String topic;

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    @Override
    public String toString() {
        return "Topics{" +
                "topic='" + topic + '\'' +
                '}';
    }
}
