package com.example.taskapp.archivoappadmin.Activites.ManagementActivity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.taskapp.archivoappadmin.Common.Common;
import com.example.taskapp.archivoappadmin.Models.GroupsTaskTime;
import com.example.taskapp.archivoappadmin.Models.GroupsTasks;
import com.example.taskapp.archivoappadmin.Models.NotificationModel.FCCResponse;
import com.example.taskapp.archivoappadmin.Models.NotificationModel.Notification;
import com.example.taskapp.archivoappadmin.Models.NotificationModel.Sender;
import com.example.taskapp.archivoappadmin.Models.NotificationModel.Topics;
import com.example.taskapp.archivoappadmin.Models.User;
import com.example.taskapp.archivoappadmin.R;
import com.example.taskapp.archivoappadmin.Remote.APIServices;
import com.example.taskapp.archivoappadmin.Remote.APIServicesToGroup;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.taskapp.archivoappadmin.Common.Common.KEY_INTENT_EXTRA_USER;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_QUERY_FILTER_GROUP_ID;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_QUERY_FILTER_STATUS;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_STATUS_PENDING;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_TABLE_GROUPS_TASK;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_TABLE_TASK;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_TABLE_TOKENS;
import static com.example.taskapp.archivoappadmin.Common.Common.KEY_TABLE_USERS;

public class AddTaskToGroup extends AppCompatActivity {

    private static final String TAG = "AddTaskToGroup";

    List<User> ListOfUsers = new ArrayList<>();

    private TextView tv_group_name, tv_back, tv_assignnow;
    private EditText edt_task, edt_time_admintaskactvy;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference TaskGroups_datDatabaseReference;
    ValueEventListener usersValueEventListener;

    Query query;
    APIServices mServices;
    int mHour, mMinute, mYear, mMonth, mDay;
    long millis;
    String taskDate, date_time;
    HashMap<String, GroupsTaskTime> hash;
    String groupId;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task_to_group);

        firebaseDatabase = FirebaseDatabase.getInstance();


        TaskGroups_datDatabaseReference = firebaseDatabase.getReference(KEY_TABLE_GROUPS_TASK);
        edt_time_admintaskactvy = findViewById(R.id.edt_time);

        mServices = Common.getFCMService();

        if (getIntent() != null) {
            groupId = getIntent().getStringExtra(KEY_QUERY_FILTER_GROUP_ID);

            hash = new HashMap<>();
            for (User user : ListOfUsers) {
                GroupsTaskTime groupsTaskTime = new GroupsTaskTime();
                hash.put(user.getUserUid(), groupsTaskTime);
            }


            Log.d(TAG, "onCreate: " + ListOfUsers.size());
            edt_time_admintaskactvy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    datePicker();

                }
            });
            tv_assignnow = findViewById(R.id.tv_assignNow_Group);


            edt_task = findViewById(R.id.edt_writeTask);

            tv_assignnow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //   AssignTaskToGroup(Task);

                    String Task = edt_task.getText().toString();


                    AssignTaskToGroup(Task);
                    finish();
                }
            });
        }

        //     new Database(this).fillData();

    }

    private void AssignTaskToGroup(String task) {

        GroupsTasks groupsTasks = new GroupsTasks();
        String Current_Task = String.valueOf(System.currentTimeMillis());

        groupsTasks.setDeadline(millis);
        groupsTasks.setTask(task);
        groupsTasks.setStatus(KEY_STATUS_PENDING);
        groupsTasks.setGroupId(groupId);
        groupsTasks.setTimeTable(hash);


        TaskGroups_datDatabaseReference.child(Current_Task)
                .setValue(groupsTasks)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Log.d(TAG, "onComplete: task is " + task.isSuccessful());
                    }
                });

        sendNotificationToGroup(Common.CurrentStaff.getUserName(), groupId);
        Toast.makeText(AddTaskToGroup.this, "Task has been sent to group ", Toast.LENGTH_SHORT).show();


    }

    private void datePicker() {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {


                        date_time = year + "/" + (monthOfYear + 1) + "/" + dayOfMonth;
                        //*************Call Time Picker Here ********************
                        timePicker();
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    private void timePicker() {
        // Get Current Time
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        final TimePickerDialog timePickerDialog = new TimePickerDialog(AddTaskToGroup.this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                        mHour = hourOfDay;
                        mMinute = minute;
                        taskDate = date_time + " " + mHour + ":" + mMinute;


                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
                        Date date = null;
                        try {
                            date = sdf.parse(taskDate);
                            millis = date.getTime();
                            long time = System.currentTimeMillis();
                            if (millis < time) {
                                Toast.makeText(AddTaskToGroup.this, " Select time in future..", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            edt_time_admintaskactvy.setText(taskDate);
                            Log.i("Log", " millis: " + millis);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    }
                }, mHour, mMinute, true);

        timePickerDialog.show();

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (query != null)
            query.removeEventListener(usersValueEventListener);
    }


    private void sendNotificationToGroup(final String Key, final String groupId) {

        Topics topic = new Topics();
        topic.setTopic(groupId);
        //Making raw Payload
        Notification notification = new Notification("Your group have new Task from  " + Key, "Arvhivo App");
        Sender content = new Sender("/topics/" + groupId, notification);
        Log.d(TAG, "sendNotificationToGroup: " + content.toString());
        mServices.sendNotification(content).enqueue(new Callback<FCCResponse>() {
            @Override
            public void onResponse(Call<FCCResponse> call, Response<FCCResponse> response) {
                assert response.body() != null;
                if (response.body().success == 1) {
                    Toast.makeText(AddTaskToGroup.this, "Task Assigned Successfuly ", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "onResponse: " + "Task Assigned Successfuly");

                } else {
                    Toast.makeText(AddTaskToGroup.this, " Task Assigned Successfuly,but failed to send notification", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "onResponse: " + "Task Assigned Successfuly");
                }
            }

            @Override
            public void onFailure(Call<FCCResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });
    }
}


